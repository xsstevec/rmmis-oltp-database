ALTER SESSION SET CURRENT_SCHEMA = rmmispods;

create or replace
package PKG_PROVIDER
authid current_user
as
   -----------------------------------------------------------------------
   --
   -----------------------------------------------------------------------
   procedure P_GET_PTDB_BY_NPI_RC(p_Npi       in varchar2,
                                  p_RefCursor out sys_refcursor);

   procedure P_GET_PTDB_BY_NPI_NAME_RC(p_Npi       in varchar2,
                                       p_Firstname in varchar2,
                                       p_LastName  in varchar2,
                                       p_OrgnName  in varchar2,
                                       p_RefCursor out sys_refcursor);

   procedure P_VERIFY_IN_CLIA(p_CliaNum   in varchar2,
                              p_RefCursor out sys_refcursor);

   procedure P_GET_PERSON_DATA_RC(p_Npi       in varchar2,
                                  p_UserName  in varchar2,
                                  p_UserRole  in varchar2,
                                  p_RefCursor out sys_refcursor);

   ---------------------------------------------------------------------
   -- The following procedures are used to import Provider information
   -- after enrollment and adjudication have completed.
   ---------------------------------------------------------------------
   procedure p_Insert_Entity(p_FirstName                 in varchar2,
                             p_MiddleName                in varchar2,
                             p_LastName                  in varchar2,
                             p_Suffix                    in varchar2,
                             p_OrgnName                  in varchar2,
                             p_DBA_Name                  in varchar2,
                             p_Years_DBA                 in varchar2,
                             p_Former_DBA_Name           in varchar2,
                             p_Years_Former_DBA          in varchar2,
                             p_TIN                       in varchar2,
                             p_EIN                       in varchar2,
                             p_SSN                       in varchar2,
                             p_Gender                    in varchar2,
                             p_DOB                       in varchar2,
                             p_UserName                  in varchar2,
                             p_UserRole                  in varchar2,
                             p_TransactionId             in varchar2,
                             p_EntityId                  out varchar2);

   procedure P_INSERT_PROVIDER(p_FirstName                 in varchar2,
                               p_MiddleName                in varchar2,
                               p_LastName                  in varchar2,
                               p_Suffix                    in varchar2,
                               p_Title                     in varchar2,
                               p_DBA_Name                  in varchar2,
                               p_Years_DBA                 in varchar2,
                               p_Former_DBA_Name           in varchar2,
                               p_Years_Former_DBA          in varchar2,
                               p_TIN                       in varchar2,
                               p_EIN                       in varchar2,
                               p_SSN                       in varchar2,
                               p_NPI                       in varchar2,
                               p_Gender                    in varchar2,
                               p_DOB                       in varchar2,
                               p_Provider_Email_Address    in varchar2,
                               p_Ownership_Type            in varchar2,
                               p_Enrolled_In_Medicaid      in varchar2,
                               p_Using_Other_Medicaid_CHIP in varchar2,
                               p_Medicare_ID               in varchar2,
                               p_Medicare_NPI              in varchar2,
                               p_Medicare_Effective_Date   in varchar2,
                               p_State                     in varchar2,
                               p_Other_Medicaid_ID         in varchar2,
                               p_Other_Medicaid_NPI        in varchar2,
                               p_Other_Medicaid_Eff_Date   in varchar2,
                               p_Other_Medicaid_State      in varchar2,
                               p_PrescMed                  in varchar2,
                               p_PrescCov                  in varchar2,
                               p_BoardCertInd              in varchar2,
                               p_SpecPermitInd             in varchar2,
                               p_Education                 in varchar2,
                               p_TransactionId             in varchar2,
                               p_NewProvInd                out varchar2, -- 'New', or 'Exist'
                               p_ProviderID                out number);  -- Will return NULL/blank if not found

   procedure P_INSERT_ADDRESS(p_Provider_Id         in number,
                              p_Address_Type        in varchar2,
                              p_Street              in varchar2,
                              p_Street2             in varchar2,
                              p_City                in varchar2,
                              p_State_Province      in varchar2,
                              p_Zip_Code            in varchar2,
                              p_County              in varchar2,
                              p_ContactName         in varchar2,
                              p_Care_Of             in varchar2,
                              p_Telephone_Number    in varchar2,
                              p_Telephone_Extension in varchar2,
                              p_Fax                 in varchar2,
                              p_EmailAddr           in varchar2,
                              p_Practice_Name       in varchar2,
--                              p_UserName            in varchar2,
--                              p_UserRole            in varchar2,
                              p_TransactionId       in varchar2,
                              p_AddressID           out number);  -- Will return NULL/blank if not found

   procedure P_INSERT_CERTIFICATION(p_Provider_Id     in number,
                                    p_CertType        in varchar2,
                                    p_CertEntity      in varchar2,
                                    p_CertNumber      in varchar2,
                                    p_EffectiveDate   in varchar2,
                                    p_ExpirationDate  in varchar2,
                                    p_CertDocId       in varchar2,
                                    p_CertDocFileName in varchar2,
                                    p_TransactionId   in varchar2,
                                    p_SuccessInd     out varchar2);

   procedure P_INSERT_CLIA(p_Provider_Id     in number,
                           p_SiteLoc         in varchar2,
                           p_CLIACertType    in varchar2,
                           p_CertNumber      in varchar2,
                           p_EffectiveDate   in varchar2,
                           p_ExpirationDate  in varchar2,
                           p_UserName        in varchar2,
                           p_UserRole        in varchar2,
                           p_TransactionId   in varchar2,
                           p_SuccessInd     out varchar2);

   procedure P_INSERT_LICENSE(p_Provider_Id     in number,
                              p_LicenseEntity   in varchar2,
                              p_LicenseNumber   in varchar2,
                              p_LicenseType     in varchar2,
                              p_LicenseCategory in varchar2,
                              p_LicenseDesc     in varchar2,
                              p_State           in varchar2,
                              p_LicnEffDate     in varchar2, -- Format as 'MM/DD/YYYY'
                              p_LicnExpDate     in varchar2, -- Format as 'MM/DD/YYYY'
                              p_LicnDocId       in varchar2,
                              p_LicnDocFileName in varchar2,
                              p_TransactionId   in varchar2,
                              p_SuccessInd     out varchar2);

   procedure P_INSERT_OFFICE_HOURS(p_Provider_Id   in number,
                                   p_SiteLoc       in varchar2,
                                   p_WorkingDay    in varchar2,
                                   p_FromTime      in varchar2,
                                   p_ToTime        in varchar2,
                                   p_Open24hrInd   in varchar2,
                                   p_ClosedInd     in varchar2,
                                   p_TransactionId in varchar2,
                                   p_SuccessInd    out varchar2);

   procedure P_INSERT_AFFILIATION(p_Provider_Id   in number,
                                  p_AgencyProvId  in varchar2,
                                  p_Npi           in varchar2,
                                  p_OrgnName      in varchar2,
                                  p_TransactionId in varchar2,
                                  p_SuccessInd    out varchar2);

   procedure P_INSERT_OWNERSHIP(p_Provider_Id   in number,
                                p_OwnerTypeCode in varchar2,
                                p_FirstName     in varchar2,
                                p_MiddleName    in varchar2,
                                p_LastName      in varchar2,
                                p_Ssn           in varchar2,
                                p_BirthDate     in varchar2,
                                p_BusName       in varchar2,
                                p_Ein           in varchar2,
                                p_OwnPerct      in varchar2,
                                p_OwnNumShares  in varchar2,
                                p_OwnRelCode    in varchar2,
                                p_TransactionId in varchar2,
                                p_SuccessInd    out varchar2);

   procedure P_INSERT_MANAGE_REL(p_Provider_Id   in number,
                                 p_OrgnName      in varchar2,
                                 p_Ein           in varchar2,
                                 p_FirstName     in varchar2,
                                 p_MiddleName    in varchar2,
                                 p_LastName      in varchar2,
                                 p_Suffix        in varchar2,
                                 p_Ssn           in varchar2,
                                 p_BirthDate     in varchar2,
                                 p_BusRelCode    in varchar2,
                                 p_FamRelCode    in varchar2,
                                 p_OtherExplain  in varchar2,
                                 p_TransactionId in varchar2,
                                 p_SuccessInd    out varchar2);

   procedure P_INSERT_PROVIDER_CONTACT(p_Provider_Id   in number,
                                       p_SiteLoc       in varchar2,
                                       p_ContactName   in varchar2,
                                       p_EmailAddr     in varchar2,
                                       p_TransactionId in varchar2,
                                       p_UserName      in varchar2,
                                       p_UserRole      in varchar2,
                                       p_SuccessInd    out varchar2);

end PKG_PROVIDER;
/

show errors;

create or replace package body PKG_PROVIDER as
   ----------------------------------------------------------------
   -- Private global variables
   ----------------------------------------------------------------
   g_TraceMsg varchar2(155);
   g_Rowid    rowid;  

   x_CustomError exception;
   PRAGMA EXCEPTION_INIT(x_CustomError, -20001 );

   ----------------------------------------------------------------
   -- Private procedures/functions
   ----------------------------------------------------------------
   function f_Npi_Lookup(p_Npi varchar2)
   return number
   is
      v_ProvId number := 0;

   begin
      g_TraceMsg := 'f_Npi_Lookup: Check NPI length';
      if length(p_Npi) <= 0
      then
         return -1;
      end if;

      g_TraceMsg := 'f_Npi_Lookup: ID_TYPE_PROVIDER table lookup';
      select EXID_PRVD_FK
      into v_ProvId
      from RMMISPODS.ID_TYPE_PROVIDER a
      where EXID_EXTERNAL_ID = p_Npi
      and   EXID_IDTP        = 'NPI';

      return v_ProvId;

   exception
      when no_data_found then
         return -1;
      when others then
         return -1;
   end f_Npi_Lookup;

   ----------------------------------------------------------------
   -- Public procedures/functions
   ----------------------------------------------------------------

   ----------------------------------------------------------------
   procedure P_GET_PTDB_BY_NPI_RC(p_Npi       in varchar2,
                                  p_RefCursor out sys_refcursor)
   as
      v_ProvId PROVIDER.prvd_id_pk%type;

   begin
      -- Lookup provider ID using NPI.
      v_ProvId := f_Npi_Lookup(p_Npi);

      -- Create reference cursor to pass back.
      open p_RefCursor for
      select exid_external_id        as "NPI",
             enty_first_name         as "First Name",
             enty_middle_name        as "Middle Name",
             enty_last_name          as "Last Name",
             enty_orgn_name          as "Organization Name",
             addr_line_1             as "Address Line 1",
             addr_line_2             as "Address Line 2",
             addr_city               as "City",
             (select stat_code
              from REF_STATE
              where stat_id_pk = 
                    addr_stat_id_fk) as "State",
             addr_zip_code           as "Zip Code",
             excl_status             as "Exclusion Status",
             excl_comments           as "Exclusion Comments",
             excl_eff_start_date     as "Exclusion Start Date",
             excl_eff_end_date       as "Exclusion End Date",
             (select ptyp_code_desc
              from REF_PROVIDER_TYPE
              where ptyp_id_pk =
                    pinf_ptyp_fk) as "Provider Type"
      from PROVIDER
      join PROVIDER_INFORMATION_DETAIL
         on pinf_prvd_fk = prvd_id_pk
      join ENTITY
          on enty_pk = prvd_enty_fk
      join EXCLUSIONS_PROVIDER
          on excl_prvd_fk = prvd_id_pk
      join PROVIDER_ADDRESS
          on padr_prvd_fk = prvd_id_pk
         and padr_atyp_fk = (select atyp_id_pk
                             from REF_ADDRESS_TYPE
                             where atyp_code = '01')
      join ADDRESS
          on addr_id_pk = padr_addr_fk
      left outer join ID_TYPE_PROVIDER
          on exid_prvd_fk = prvd_id_pk
         and exid_idtp    = 'NPI'
      where prvd_id_pk = v_ProvId;
      return;

   exception
      when no_data_found then
         raise;
      when others then
         raise;
   end P_GET_PTDB_BY_NPI_RC;

   ----------------------------------------------------------------
   procedure P_GET_PTDB_BY_NPI_NAME_RC(p_Npi       in varchar2,
                                       p_Firstname in varchar2,
                                       p_LastName  in varchar2,
                                       p_OrgnName  in varchar2,
                                       p_RefCursor out sys_refcursor)
   as

   begin
      -- If NPI is provided, then call P_GET_PTDB_BY_NPI_RC
      if length(nvl(p_Npi, '')) > 0
      then
         P_GET_PTDB_BY_NPI_RC(p_Npi,
                              p_RefCursor);
         return;
      end if;

      open p_RefCursor for 
      select exid_external_id        as "NPI",
             enty_first_name         as "First Name", 
             enty_middle_name        as "Middle Name", 
             enty_last_name          as "Last Name", 
             enty_orgn_name          as "Organization Name",
             addr_line_1             as "Address Line 1",
             addr_line_2             as "Address Line 2",
             addr_city               as "City",
             (select stat_code
              from REF_STATE
              where stat_id_pk = 
                    addr_stat_id_fk) as "State",
             addr_zip_code           as "Zip Code",
             excl_status             as "Exclusion Status",
             excl_comments           as "Exclusion Comments",
             excl_eff_start_date     as "Exclusion Start Date",
             excl_eff_end_date       as "Exclusion End Date",
             (select ptyp_code_desc
              from REF_PROVIDER_TYPE
              where ptyp_id_pk =
                    pinf_ptyp_fk)    as "Provider Type"
      from PROVIDER
      join PROVIDER_INFORMATION_DETAIL
         on pinf_prvd_fk = prvd_id_pk
      join ENTITY
          on enty_pk = prvd_enty_fk
         and upper(enty_first_name) like '%'||upper(p_Firstname)||'%'
         and upper(enty_last_name)  like '%'||upper(p_LastName)||'%'
         and upper(enty_orgn_name)  like '%'||upper(p_OrgnName)||'%'
      join EXCLUSIONS_PROVIDER
          on excl_prvd_fk = prvd_id_pk
      join PROVIDER_ADDRESS
          on padr_prvd_fk = prvd_id_pk
         and padr_atyp_fk = (select atyp_id_pk
                            from REF_ADDRESS_TYPE
                            where atyp_code = '01')
      join ADDRESS
          on addr_id_pk = padr_addr_fk
      left outer join ID_TYPE_PROVIDER
          on exid_prvd_fk = prvd_id_pk
         and exid_idtp = 'NPI';
      return;

   exception
      when no_data_found then
         raise;
      when others then
         raise;
   end P_GET_PTDB_BY_NPI_NAME_RC;

   procedure P_VERIFY_IN_CLIA(p_CliaNum   in varchar2,
                              p_RefCursor out sys_refcursor)	
   is

   begin

      open p_RefCursor for
      select SITE_NUMBER,
             ADDR_LINE_1,
             ADDR_LINE_2,
             NULL,
             ADDR_CITY,
             (select stat_code
              from REF_STATE
              where stat_id_pk = ADDR_STAT_ID_FK),
             ADDR_ZIP_CODE,
             CLIA_NUM,
             CLIA_PAID_IND,
             CLIA_EFF_DATE,
             CLIA_EXP_DATE,
             CCRT_CERT_TYPE,
             CCRT_CERT_NUM,
             CCRT_FACL_TYPE,
             CCRT_EFF_DATE,
             CCRT_EXP_DATE,
             CSPC_CODE,
             CSPC_EFF_DATE,
             CSPC_EXP_DATE
      from CLIA_CERT_SPECIALTY_SITE
      join PROVIDER_SITE
          on SITE_ID = CLIA_SITE_FK
      join PROVIDER_SITE_ADDRESS
          on sadr_site_fk = site_id
      join ADDRESS
          on ADDR_ID_PK = sadr_addr_id_fk
      join CLIA_CERTIFICATION
          on CCRT_CLIA_FK = CLIA_ID_PK
      join CLIA_SPECIALTY
          on CSPC_CLIA_FK = CLIA_ID_PK
      where clia_num = p_CliaNum;

      return;

   exception
      when no_data_found then
         raise;
      when others then
         raise;
   end P_VERIFY_IN_CLIA;

   ---------------------------------------------------------------------
   -- This is a test procedure for dynamic redaction policies
   ---------------------------------------------------------------------
   procedure P_GET_PERSON_DATA_RC(p_Npi       in varchar2,
                                  p_UserName  in varchar2,
                                  p_UserRole  in varchar2,
                                  p_RefCursor out sys_refcursor)
   is
      v_EntityPk entity.enty_pk%type;

   begin
      g_TraceMsg := 'P_GET_PERSON_DATA_RC: Clear session role';
      execute immediate 'set role none';

      g_TraceMsg := 'P_GET_PERSON_DATA_RC: Set session role';
      execute immediate 'set role '||p_UserRole;

      g_TraceMsg := 'P_GET_PERSON_DATA_RC: Get entity pk from NPI';
      select prvd_enty_fk
      into v_EntityPk
      from provider
      join id_type_provider
          on exid_prvd_fk     = prvd_id_pk
         and exid_idtp        = 'NPI'
         and exid_external_id = p_Npi;

      g_TraceMsg := 'P_GET_PERSON_DATA_RC: Open sys_refcursor';
      open p_RefCursor for
      select ENTY_LAST_NAME,
             ENTY_FIRST_NAME,
             ENTY_MIDDLE_NAME,
             ENTY_NAME_PREFIX,
             ENTY_NAME_SUFFIX,
             ENTY_SSN,
             ENTY_BIRTH_DATE
      from entity
      where enty_pk = v_EntityPk;

      return;

   exception
      when no_data_found then
         dbms_output.put_line(g_TraceMsg);
         raise;
      when others then
         dbms_output.put_line(g_TraceMsg);
         raise;
   end P_GET_PERSON_DATA_RC;

   ---------------------------------------------------------------------
   procedure p_Insert_Entity(p_FirstName                 in varchar2,
                             p_MiddleName                in varchar2,
                             p_LastName                  in varchar2,
                             p_Suffix                    in varchar2,
                             p_OrgnName                  in varchar2,
                             p_DBA_Name                  in varchar2,
                             p_Years_DBA                 in varchar2,
                             p_Former_DBA_Name           in varchar2,
                             p_Years_Former_DBA          in varchar2,
                             p_TIN                       in varchar2,
                             p_EIN                       in varchar2,
                             p_SSN                       in varchar2,
                             p_Gender                    in varchar2,
                             p_DOB                       in varchar2,
                             p_UserName                  in varchar2,
                             p_UserRole                  in varchar2,
                             p_TransactionId             in varchar2,
                             p_EntityId                  out varchar2)
   is
      v_EntityRow  ENTITY%rowtype;

   begin
      -- Check to see if the entity already exists. If so, then
      -- return the entity ID. If not, continue on and insert
      -- the entity.
      begin
         g_TraceMsg := 'p_Insert_Entity: Get entity pk from SSN';
         select ENTY_PK
         into p_EntityId
         from (select enty_pk
               from entity
               where enty_ssn = p_SSN
               union
               select enty_pk
               from entity
               where enty_ein = p_EIN
               union
               select enty_pk
               from entity
               where enty_tin = p_TIN)
         where rownum = 1;

         return;

      exception
         when no_data_found then
            null;
         when others then
            raise;
      end;

      if length(nvl(p_Gender, '')) > 0
      then
         -- Retrieve the gender foreign key
         g_TraceMsg := 'p_Insert_Entity: Get gender code';
         with curr_rec as
         (select gndr_code                as curr_code,
                 max(gndr_eff_start_date) as curr_eff_date
          from ref_gender
          where gndr_eff_start_date <= sysdate
          and   gndr_code            = p_Gender
          group by gndr_code
         )
         select gndr_id_pk
         into v_EntityRow.ENTY_GNDR_FK
         from ref_gender
         join curr_rec
             on gndr_code           = curr_code
            and gndr_eff_start_date = curr_eff_date;
      else
         v_EntityRow.ENTY_GNDR_FK := null;
      end if;

      -- Set the entity type as either individual or corporation
      g_TraceMsg := 'p_Insert_Entity: Set entity type';
      if length(nvl(p_FirstName, '')) = 0
      then
         v_EntityRow.ENTY_TYPE_CODE := 'C';
      else
         v_EntityRow.ENTY_TYPE_CODE := 'P';
      end if;

      g_TraceMsg := 'p_Insert_Entity: Set entity ID';
      p_EntityId := RMMISPODS.ETDENTY_ENTY_PK_SEQ.nextval;

      g_TraceMsg := 'p_Insert_Entity: Set entity row';
      v_EntityRow.ENTY_PK             := p_EntityId;
      v_EntityRow.ENTY_CREATION_DATE  := sysdate;
      v_EntityRow.ENTY_LAST_NAME      := p_LastName;
      v_EntityRow.ENTY_FIRST_NAME     := p_FirstName;
      v_EntityRow.ENTY_MIDDLE_NAME    := p_MiddleName;
      v_EntityRow.ENTY_NAME_PREFIX    := NULL;
      v_EntityRow.ENTY_NAME_SUFFIX    := p_Suffix;
      v_EntityRow.ENTY_SSN            := p_SSN;
      v_EntityRow.ENTY_BIRTH_DATE     := to_date(p_DOB, 'MMDDYYYY');
      v_EntityRow.ENTY_DEATH_DATE     := NULL;
      v_EntityRow.ENTY_ETHN_FK        := NULL;
      v_EntityRow.ENTY_CORP_NAME      := NULL;
      v_EntityRow.ENTY_DBA            := p_DBA_Name;
      v_EntityRow.ENTY_DBA_YEARS      := to_number(p_Years_DBA);
      v_EntityRow.ENTY_DBA_PREV       := p_Former_DBA_Name;
      v_EntityRow.ENTY_DBA_YEARS_PREV := to_number(p_Years_Former_DBA);
      v_EntityRow.ENTY_LEGAL_NAME     := NULL;
      v_EntityRow.ENTY_ORGN_NAME      := p_OrgnName;
      v_EntityRow.ENTY_TAX_NAME       := NULL;
      v_EntityRow.ENTY_EIN            := p_EIN;
      v_EntityRow.ENTY_STOCK_CODE     := NULL;
      v_EntityRow.ENTY_TIN            := p_TIN;
      v_EntityRow.ENTY_EDIT_USER_ID   := p_UserName;
      v_EntityRow.ENTY_EDIT_DATE      := sysdate;

      -- Insert new entity row
      g_TraceMsg := 'p_Insert_Entity: Insert entity row';
      insert into ENTITY values v_EntityRow returning rowid into g_Rowid;
      g_TraceMsg := 'Create transaction record for: '||g_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'ENTITY',
                                                       p_RowId         => g_Rowid);

   exception
      when no_data_found then
         dbms_output.put_line(g_TraceMsg);
         raise;
      when others then
         dbms_output.put_line(g_TraceMsg);
         raise;
   end p_Insert_Entity;

   procedure P_INSERT_PROVIDER(p_FirstName                 in varchar2,
                               p_MiddleName                in varchar2,
                               p_LastName                  in varchar2,
                               p_Suffix                    in varchar2,
                               p_Title                     in varchar2,
                               p_DBA_Name                  in varchar2,
                               p_Years_DBA                 in varchar2,
                               p_Former_DBA_Name           in varchar2,
                               p_Years_Former_DBA          in varchar2,
                               p_TIN                       in varchar2,
                               p_EIN                       in varchar2,
                               p_SSN                       in varchar2,
                               p_NPI                       in varchar2,
                               p_Gender                    in varchar2,
                               p_DOB                       in varchar2,
                               p_Provider_Email_Address    in varchar2,
                               p_Ownership_Type            in varchar2,
                               p_Enrolled_In_Medicaid      in varchar2,
                               p_Using_Other_Medicaid_CHIP in varchar2,
                               p_Medicare_ID               in varchar2,
                               p_Medicare_NPI              in varchar2,
                               p_Medicare_Effective_Date   in varchar2,
                               p_State                     in varchar2,
                               p_Other_Medicaid_ID         in varchar2,
                               p_Other_Medicaid_NPI        in varchar2,
                               p_Other_Medicaid_Eff_Date   in varchar2,
                               p_Other_Medicaid_State      in varchar2,
                               p_PrescMed                  in varchar2,
                               p_PrescCov                  in varchar2,
                               p_BoardCertInd              in varchar2,
                               p_SpecPermitInd             in varchar2,
                               p_Education                 in varchar2,
                               p_TransactionId             in varchar2,
                               p_NewProvInd                out varchar2, -- 'New', or 'Exist'
                               p_ProviderID                out number)
   is
      v_ProvIdNum  PROVIDER.PRVD_ID_PK%type;
--      v_EntityRow  ENTITY%rowtype;
      v_Provider   PROVIDER%rowtype;
      v_ProvDetail PROVIDER_INFORMATION_DETAIL%rowtype;
      v_Title      TITLE%rowtype;
      v_ExtId      ID_TYPE_PROVIDER%rowtype;
      v_Email      EMAIL%rowtype;
      v_EmailEnty  PROVIDER_EMAIL%rowtype;
      v_DegreeRec  DEGREE_PROVIDER%rowtype;
      v_EntityId   ENTITY.ENTY_PK%type;
      v_TraceMsg   varchar2(155);

   begin
      -- First thing is to check required fields are populated
      if length(nvl(p_State, '')) = 0
      then
         raise_application_error( -20001, 'Parameter P_State value missing: ->'||p_State||';');
      end if;

      if length(nvl(p_Other_Medicaid_Eff_Date, '')) = 0
      then
         raise_application_error( -20001, 'Parameter p_Other_Medicaid_Eff_Date value missing: ->'||p_Other_Medicaid_Eff_Date||';');
      end if;

      -- Check NPI to see if it is already in use. If it is, return an error.
      v_TraceMsg := 'Check NPI to see if it is already in use';
      begin
         select EXID_PRVD_FK
         into v_ProvIdNum
         from ID_TYPE_PROVIDER
         where EXID_EXTERNAL_ID = p_NPI
         and   EXID_IDTP        = 'NPI';

         p_ProviderID := v_ProvIdNum;
         p_NewProvInd := 'Exist';
         return;

      exception
         when no_data_found then
            -- We only want to proceed if the provider does not already exist
            p_NewProvInd := 'New';
      end;

      -- Retrieve the ownership type foreign key
      v_TraceMsg := 'Get ownership type code';
      with curr_rec as
      (select ownt_code                as curr_code,
              max(ownt_eff_start_date) as curr_eff_date
       from REF_PROVIDER_OWNERSHIP_TYPE
       where ownt_eff_start_date <= sysdate
       and   ownt_code            = p_Ownership_Type
       group by ownt_code
      )
      select ownt_id_pk
      into v_ProvDetail.PINF_OWNT_FK
      from REF_PROVIDER_OWNERSHIP_TYPE
      join curr_rec
          on ownt_code           = curr_code
         and ownt_eff_start_date = curr_eff_date;

      -- Retrieve the medicare state foreign key
      v_TraceMsg := 'Get state code';
      with curr_rec as
      (select stat_code                as curr_code,
              max(stat_eff_start_date) as curr_eff_date
       from REF_STATE
       where stat_eff_start_date <= sysdate
       and   stat_code            = p_State
       group by stat_code
      )
      select stat_id_pk
      into v_ProvDetail.PINF_MDCR_STATE_FK
      from REF_STATE
      join curr_rec
          on stat_code           = curr_code
         and stat_eff_start_date = curr_eff_date;

      -- Retrieve the other medicaid state foreign key
      v_TraceMsg := 'Get medicaid state code';
      with curr_rec as
      (select stat_code                as curr_code,
              max(stat_eff_start_date) as curr_eff_date
       from REF_STATE
       where stat_eff_start_date <= sysdate
       and   stat_code            = p_Other_Medicaid_State
       group by stat_code
      )
      select stat_id_pk
      into v_ProvDetail.PINF_MDCD_OTHER_STATE_FK
      from REF_STATE
      join curr_rec
          on stat_code           = curr_code
         and stat_eff_start_date = curr_eff_date;

      v_TraceMsg := 'P_INSERT_PROVIDER: Load entity';
      p_Insert_Entity(p_FirstName        => p_FirstName,
                      p_MiddleName       => p_MiddleName,
                      p_LastName         => p_LastName,
                      p_Suffix           => p_Suffix,
                      p_OrgnName         => null,
                      p_DBA_Name         => p_DBA_Name,
                      p_Years_DBA        => p_Years_DBA,
                      p_Former_DBA_Name  => p_Former_DBA_Name,
                      p_Years_Former_DBA => p_Years_Former_DBA,
                      p_TIN              => p_TIN,
                      p_EIN              => p_EIN,
                      p_SSN              => p_SSN,
                      p_Gender           => p_Gender,
                      p_DOB              => p_DOB,
                      p_UserName         => user,
                      p_UserRole         => null,
                      p_TransactionId    => p_TransactionId,
                      p_EntityId         => v_EntityId);

      -- After the ENTITY row has been inserted, we can
      -- now create the PROVIDER and PROVIDER_DETAIL
      -- records.
      v_TraceMsg := 'Set provider row';
      v_Provider.PRVD_ID_PK                 := PROVIDER_PRVD_ID_PK_SEQ.nextval;
      v_Provider.PRVD_ENTY_FK               := v_EntityId;
      v_Provider.PRVD_DEPT_ASGN_ID          := NULL;
      v_Provider.PRVD_EDIT_USER_ID          := user;
      v_Provider.PRVD_EDIT_DATE             := sysdate;
      v_TraceMsg := 'Set provider detail row';
      v_ProvDetail.PINF_ID_PK               := PTDPINF_PINF_ID_PK_SEQ.nextval;
      v_ProvDetail.PINF_PRVD_FK             := v_Provider.PRVD_ID_PK;
      v_ProvDetail.PINF_PTYP_FK             := NULL;
      v_ProvDetail.PINF_PAPP_FK             := NULL;
      v_ProvDetail.PINF_PGRP_FK             := NULL;
      v_ProvDetail.PINF_ID_CODE             := NULL;
      v_ProvDetail.PINF_HC_FIN_PLAN_ID      := NULL;
      v_ProvDetail.PINF_BUS_TYPE            := NULL;
      v_ProvDetail.PINF_MSA_IND             := NULL;
      v_ProvDetail.PINF_NONPROFIT_STAT      := NULL;
      v_ProvDetail.PINF_MDCD_ENRLL_CODE     := NULL;
      v_ProvDetail.PINF_MDCD_ENRLL_IND      := p_Enrolled_In_Medicaid;
      v_ProvDetail.PINF_MDCD_ENRLL_METH     := NULL;
      v_ProvDetail.PINF_MDCD_BEGIN_DATE     := sysdate;
      v_ProvDetail.PINF_MDCD_END_DATE       := NULL;
      v_ProvDetail.PINF_MDCR_EFF_DATE       := to_date(p_Medicare_Effective_Date, 'MMDDYYYY');
      v_ProvDetail.PINF_MDCD_OTHER_EFF_DATE := to_date(p_Other_Medicaid_Eff_Date, 'MMDDYYYY');
      v_ProvDetail.PINF_MDCD_OTHER_CHIP_IND := p_Using_Other_Medicaid_CHIP;
      v_ProvDetail.PINF_ACPT_NEWPAT_IND     := NULL;
      v_ProvDetail.PINF_PAR_IND             := NULL;
      v_ProvDetail.PINF_HH_IND              := NULL;
      v_ProvDetail.PINF_ORIG_DATA_SOURCE    := 'PROV_ENROLL';
      v_ProvDetail.PINF_INSERT_DATE         := sysdate;
      v_ProvDetail.PINF_RECORD_EFF_DATE     := sysdate;
      v_ProvDetail.PINF_RECORD_EXP_DATE     := NULL;
      v_ProvDetail.PINF_SPECIAL_PERMIT      := p_SpecPermitInd;
      v_ProvDetail.PINF_BOARD_IND           := p_BoardCertInd;
      v_ProvDetail.PINF_EDIT_USER_ID        := user;
      v_ProvDetail.PINF_EDIT_DATE           := sysdate;

      -- Insert new Provider and Provider Detail row
      v_TraceMsg := 'Insert provider row';
      insert into PROVIDER values v_Provider returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER',
                                                       p_RowId         => g_Rowid);

      v_TraceMsg := 'Insert provider detail row';
      insert into PROVIDER_INFORMATION_DETAIL values v_ProvDetail returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER_INFORMATION_DETAIL',
                                                       p_RowId         => g_Rowid);

      -- Prepare the row for the Title table insert
      if length(nvl(p_Title, '')) > 0
      then
         v_TraceMsg := 'Set title row';
         v_Title.TITL_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_Title.TITL_PROF_TITLE   := p_Title;
         v_Title.TITL_BEGIN_DATE   := sysdate;
         v_Title.TITL_END_DATE     := null;
         v_Title.TITL_EDIT_USER_ID := user;
         v_Title.TITL_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert title row';
         insert into TITLE values v_Title returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'TITLE',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for the NPI insert into external ID table
      if length(nvl(p_NPI, '')) > 0
      then
         v_TraceMsg := 'Set external ID NPI row';
         v_ExtId.EXID_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_ExtId.EXID_IDTP         := 'NPI';
         v_ExtId.EXID_EXTERNAL_ID  := p_NPI;
         v_ExtId.EXID_DESC         := 'Provider NPI number';
         v_ExtId.EXID_EFF_DATE     := sysdate;
         v_ExtId.EXID_END_DATE     := null;
         v_ExtId.EXID_EDIT_USER_ID := user;
         v_ExtId.EXID_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert external ID NPI row';
         insert into ID_TYPE_PROVIDER values v_ExtId returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ID_TYPE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for the NPI insert into external ID table
      if length(nvl(p_Medicare_NPI, '')) > 0
      then
         v_TraceMsg := 'Set external ID MDCR NPI row';
         v_ExtId.EXID_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_ExtId.EXID_IDTP         := 'MDCR_NPI';
         v_ExtId.EXID_EXTERNAL_ID  := p_Medicare_NPI;
         v_ExtId.EXID_DESC         := 'Medicare NPI number';
         v_ExtId.EXID_EFF_DATE     := sysdate;
         v_ExtId.EXID_END_DATE     := null;
         v_ExtId.EXID_EDIT_USER_ID := user;
         v_ExtId.EXID_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert external ID MDCR NPI row';
         insert into ID_TYPE_PROVIDER values v_ExtId returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ID_TYPE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for the NPI insert into external ID table
      if length(nvl(p_Other_Medicaid_NPI, '')) > 0
      then
         v_TraceMsg := 'Set external ID MDCD NPI row';
         v_ExtId.EXID_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_ExtId.EXID_IDTP         := 'MDCD_NPI';
         v_ExtId.EXID_EXTERNAL_ID  := p_Other_Medicaid_NPI;
         v_ExtId.EXID_DESC         := 'Medicaid NPI number';
         v_ExtId.EXID_EFF_DATE     := sysdate;
         v_ExtId.EXID_END_DATE     := null;
         v_ExtId.EXID_EDIT_USER_ID := user;
         v_ExtId.EXID_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert external ID MDCD NPI row';
         insert into ID_TYPE_PROVIDER values v_ExtId returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ID_TYPE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for the Medicare ID insert into external ID table
      if length(nvl(p_Medicare_ID, '')) > 0
      then
         v_TraceMsg := 'Set external ID MDCR row';
         v_ExtId.EXID_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_ExtId.EXID_IDTP         := 'MDCR';
         v_ExtId.EXID_EXTERNAL_ID  := p_Medicare_ID;
         v_ExtId.EXID_DESC         := 'Medicare ID';
         v_ExtId.EXID_EFF_DATE     := sysdate;
         v_ExtId.EXID_END_DATE     := null;
         v_ExtId.EXID_EDIT_USER_ID := user;
         v_ExtId.EXID_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert external ID MDCR row';
         insert into ID_TYPE_PROVIDER values v_ExtId returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ID_TYPE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for the Medicaid ID insert into external ID table
      if length(nvl(p_Other_Medicaid_ID, '')) > 0
      then
         v_TraceMsg := 'Set external ID MDCD row';
         v_ExtId.EXID_PRVD_FK      := v_Provider.PRVD_ID_PK;
         v_ExtId.EXID_IDTP         := 'MDCD';
         v_ExtId.EXID_EXTERNAL_ID  := p_Other_Medicaid_ID;
         v_ExtId.EXID_DESC         := 'Medicaid ID';
         v_ExtId.EXID_EFF_DATE     := sysdate;
         v_ExtId.EXID_END_DATE     := null;
         v_ExtId.EXID_EDIT_USER_ID := user;
         v_ExtId.EXID_EDIT_DATE    := sysdate;

         v_TraceMsg := 'Insert external ID MDCD row';
         insert into ID_TYPE_PROVIDER values v_ExtId returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ID_TYPE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Process educational credentials
      if length(nvl(p_Education, '')) > 0
      then
         v_TraceMsg := 'Get degree';
         with curr_rec as
         (select DEGR_CODE                as curr_code,
                 max(DEGR_EFF_START_DATE) as curr_eff_date
          from REF_DEGREE
          where DEGR_EFF_START_DATE <= sysdate
          and   DEGR_CODE            = p_Education
          group by DEGR_CODE
         )
         select DEGR_ID_PK
         into v_DegreeRec.DEGR_FK
         from REF_DEGREE
         join curr_rec
             on DEGR_CODE           = curr_code
            and DEGR_EFF_START_DATE = curr_eff_date;

         v_TraceMsg := 'P_INSERT_PROVIDER: Load degree record';
         v_DegreeRec.DEGR_PRVD_FK       := v_Provider.PRVD_ID_PK;
         v_DegreeRec.DEGR_LEVEL         := null;
         v_DegreeRec.DEGR_AREA_OF_STUDY := null;
         v_DegreeRec.DEGR_EDIT_USER_ID  := user;
         v_DegreeRec.DEGR_EDIT_DATE     := sysdate;

         -- Insert license type record
         v_TraceMsg := 'P_INSERT_PROVIDER: Inserting degree record';
         insert into DEGREE_PROVIDER values v_DegreeRec returning rowid into g_Rowid;

         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'DEGREE_PROVIDER',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Prepare the row for email address
      if length(nvl(p_Provider_Email_Address, '')) > 0
      then
      begin
         select emal_id
         into v_Email.EMAL_ID
         from EMAIL
         where emal_address = p_Provider_Email_Address;

      exception
         when no_data_found then
            v_TraceMsg := 'Set email row';
            v_Email.EMAL_ID           := EMAIL_EMAL_ID_SEQ.nextval;
            v_Email.EMAL_ADDRESS      := p_Provider_Email_Address;
            v_Email.EMAL_EDIT_USER_ID := user;
            v_Email.EMAL_EDIT_DATE    := sysdate;
   
            v_TraceMsg := 'Insert email row';
            insert into EMAIL values v_Email returning rowid into g_Rowid;
            v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
            PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                             p_TableOwner    => 'RMMISPODS',
                                                             p_TableName     => 'EMAIL',
                                                             p_RowId         => g_Rowid);
         when others then
            raise;
         end;

         -- Retrieve the email type foreign key
         v_TraceMsg := 'Get email type code';
         with curr_rec as
         (select etyp_code                as curr_code,
                 max(etyp_eff_start_date) as curr_eff_date
          from REF_EMAIL_TYPE
          where etyp_eff_start_date <= sysdate
          and   etyp_code            = 'BUS'
          group by etyp_code
         )
         select etyp_id_pk
         into v_EmailEnty.PEML_EMAL_ETYP_FK
         from REF_EMAIL_TYPE
         join curr_rec
             on etyp_code           = curr_code
            and etyp_eff_start_date = curr_eff_date;

         v_TraceMsg := 'Set provider email row';
         v_EmailEnty.PEML_PRVD_FK          := v_Provider.PRVD_ID_PK;
         v_EmailEnty.PEML_EMAL_FK          := v_Email.EMAL_ID;
         v_EmailEnty.PEML_ASSGN_START_DATE := sysdate;
         v_EmailEnty.PEML_ASSGN_END_DATE   := null;
         v_EmailEnty.PEML_EDIT_USER_ID     := user;
         v_EmailEnty.PEML_EDIT_DATE        := sysdate;

         v_TraceMsg := 'Insert provider email row';
         insert into PROVIDER_EMAIL values v_EmailEnty returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'PROVIDER_EMAIL',
                                                          p_RowId         => g_Rowid);
      end if;

      p_ProviderID := v_Provider.PRVD_ID_PK;

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg);
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg);
         raise;
   end P_INSERT_PROVIDER;

   procedure P_INSERT_ADDRESS(p_Provider_Id         in number,
                              p_Address_Type        in varchar2,
                              p_Street              in varchar2,
                              p_Street2             in varchar2,
                              p_City                in varchar2,
                              p_State_Province      in varchar2,
                              p_Zip_Code            in varchar2,
                              p_County              in varchar2,
                              p_ContactName         in varchar2,
                              p_Care_Of             in varchar2,
                              p_Telephone_Number    in varchar2,
                              p_Telephone_Extension in varchar2,
                              p_Fax                 in varchar2,
                              p_EmailAddr           in varchar2,
                              p_Practice_Name       in varchar2,
--                              p_UserName            in varchar2,
--                              p_UserRole            in varchar2,
                              p_TransactionId       in varchar2,
                              p_AddressID           out number)  -- Will return NULL/blank if not found
   is
      v_AddrIdNum ADDRESS.ADDR_ID_PK%type;
      v_AddrRec   ADDRESS%rowtype;
      v_PaddrRec  PROVIDER_ADDRESS%rowtype;
      v_TeleRec   TELEPHONE%rowtype;
      v_PtelRec   PROVIDER_TELEPHONE%rowtype;
      v_EmailRec  EMAIL%rowtype;
      v_PemalRec  PROVIDER_EMAIL%rowtype;
      vPsiteRec   PROVIDER_SITE%rowtype;
      v_TraceMsg  varchar2(155);

   begin
      -- Valid Address type values:
      --    "Provider_Address"
      --    "Billing_Address"
      --    "Mailing_Address"
      --    "Pay_To_Address"
      --
      -- Retrieve the state foreign key
      v_TraceMsg := 'Get state code';
      with curr_rec as
      (select stat_code                as curr_code,
              max(stat_eff_start_date) as curr_eff_date
       from REF_STATE
       where stat_eff_start_date <= sysdate
       and   stat_code            = p_State_Province
       group by stat_code
      )
      select stat_id_pk
      into v_AddrRec.ADDR_STAT_ID_FK
      from REF_STATE
      join curr_rec
          on stat_code           = curr_code
         and stat_eff_start_date = curr_eff_date;

      -- Retrieve the address type foreign key
      v_TraceMsg := 'Get address type';
      with curr_rec as
      (select atyp_code                as curr_code,
              max(atyp_eff_start_date) as curr_eff_date
       from REF_ADDRESS_TYPE
       where atyp_eff_start_date <= sysdate
       and   atyp_code            = p_Address_Type
       group by atyp_code
      )
      select atyp_id_pk
      into v_PaddrRec.PADR_ATYP_FK
      from REF_ADDRESS_TYPE
      join curr_rec
          on atyp_code           = curr_code
         and atyp_eff_start_date = curr_eff_date;

      -- Assign values
      v_TraceMsg := 'Assign address values';
      v_AddrRec.ADDR_ID_PK          := ETDADDR_ADDR_ID_PK_SEQ.nextval;
      v_AddrRec.ADDR_LINE_1         := p_Street;
      v_AddrRec.ADDR_LINE_2         := p_Street2;
      v_AddrRec.ADDR_CITY           := p_City;
      v_AddrRec.ADDR_ZIP_CODE       := p_Zip_Code;
      v_AddrRec.ADDR_COUNTY         := p_County;
      v_AddrRec.ADDR_NATION         := null;
--      v_AddrRec.ADDR_EDIT_USER_ID   := p_UserName;
      v_AddrRec.ADDR_EDIT_USER_ID   := user;
      v_AddrRec.ADDR_EDIT_DATE      := sysdate;

      begin
         -- Insert record
         v_TraceMsg := 'Insert address record';
         insert into ADDRESS values v_AddrRec returning rowid into g_Rowid;

         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'ADDRESS',
                                                          p_RowId         => g_Rowid);

      exception
         when DUP_VAL_ON_INDEX then
            -- If the address already exists, then retrieve the PK
            select addr_id_pk
            into v_AddrRec.ADDR_ID_PK
            from address
            where ADDR_LINE_1   = v_AddrRec.ADDR_LINE_1
            and   ADDR_LINE_2   = v_AddrRec.ADDR_LINE_2
            and   ADDR_CITY     = v_AddrRec.ADDR_CITY
            and   ADDR_ZIP_CODE = v_AddrRec.ADDR_ZIP_CODE;
         when others then
            raise;
      end;

      p_AddressID := v_AddrRec.ADDR_ID_PK;

      -- Insert the Provider-Address mapping record
      v_TraceMsg := 'Assign Provider address values';
      v_PaddrRec.PADR_PRVD_FK          := p_Provider_Id;
      v_PaddrRec.PADR_ADDR_FK          := v_AddrRec.ADDR_ID_PK;
      v_PaddrRec.PADR_ATTN_LINE        := p_ContactName;
      v_PaddrRec.PADR_CARE_OF          := p_Care_Of;
      v_PaddrRec.PADR_ASSGN_START_DATE := sysdate;
      v_PaddrRec.PADR_ASSGN_END_DATE   := null;
--      v_PaddrRec.PADR_EDIT_USER_ID     := p_UserName;
      v_PaddrRec.PADR_EDIT_USER_ID     := user;
      v_PaddrRec.PADR_EDIT_DATE        := sysdate;

      -- Insert record
      v_TraceMsg := 'Insert provider address record';
      insert into PROVIDER_ADDRESS values v_PaddrRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER_ADDRESS',
                                                       p_RowId         => g_Rowid);

      begin
         select tele_id
         into v_TeleRec.TELE_ID
         from TELEPHONE
         where TELE_NUMBER              = p_Telephone_Number
         and   nvl(TELE_AREA_CODE, 'x') = nvl(NULL, 'x')
         and   nvl(TELE_EXT, 'x')       = nvl(p_Telephone_Extension, 'x');

      exception
         when no_data_found then
            -- Telephone record
            v_TraceMsg := 'Assign telephone record';
            v_TeleRec.TELE_ID             := TELEPHONE_TELE_ID_SEQ.nextval;
            v_TeleRec.TELE_COUNTRY_ACC    := null;
            v_TeleRec.TELE_AREA_CODE      := null;
            v_TeleRec.TELE_NUMBER         := p_Telephone_Number;
            v_TeleRec.TELE_EXT            := p_Telephone_Extension;
            v_TeleRec.TELE_EFF_DATE_START := sysdate;
            v_TeleRec.TELE_EFF_DATE_END   := null;
--            v_TeleRec.TELE_EDIT_USER_ID   := p_UserName;
            v_TeleRec.TELE_EDIT_USER_ID   := user;
            v_TeleRec.TELE_EDIT_DATE      := sysdate;

            -- Insert record
            v_TraceMsg := 'Insert telephone record';
            insert into TELEPHONE values v_TeleRec returning rowid into g_Rowid;
            v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
            PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                             p_TableOwner    => 'RMMISPODS',
                                                             p_TableName     => 'TELEPHONE',
                                                             p_RowId         => g_Rowid);
         when others then
            raise;
      end;

      -- Telephone assignment record
      -- Retrieve the telephone type foreign key
      v_TraceMsg := 'Get telephone type';
      with curr_rec as
      (select ttyp_code                as curr_code,
              max(ttyp_eff_start_date) as curr_eff_date
       from REF_TELEPHONE_TYPE
       where ttyp_eff_start_date <= sysdate
       and   ttyp_code            = 'OFFICE'
       group by ttyp_code
      )
      select ttyp_id_pk
      into v_PtelRec.PTEL_TELE_TTYP_FK
      from REF_TELEPHONE_TYPE
      join curr_rec
          on ttyp_code           = curr_code
         and ttyp_eff_start_date = curr_eff_date;

      v_TraceMsg := 'Assign provider telephone record';
      v_PtelRec.PTEL_PRVD_FK          := p_Provider_Id;
      v_PtelRec.PTEL_TELE_FK          := v_TeleRec.TELE_ID;
      v_PtelRec.PTEL_ASSGN_START_DATE := sysdate;
      v_PtelRec.PTEL_ASSGN_END_DATE   := null;
--      v_PtelRec.PTEL_EDIT_USER_ID     := p_UserName;
      v_PtelRec.PTEL_EDIT_USER_ID     := user;
      v_PtelRec.PTEL_EDIT_DATE        := sysdate;

      -- Insert record
      v_TraceMsg := 'Insert provider telephone record';
      insert into PROVIDER_TELEPHONE values v_PtelRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER_TELEPHONE',
                                                       p_RowId         => g_Rowid);

      if length(nvl(p_Fax, '')) > 0
      then
      begin
         select tele_id
         into v_TeleRec.TELE_ID
         from TELEPHONE
         where TELE_NUMBER              = p_Telephone_Number
         and   nvl(TELE_AREA_CODE, 'x') = nvl(NULL, 'x');

      exception
         when no_data_found then
            -- Fax record
            v_TraceMsg := 'Assign fax record';
            v_TeleRec.TELE_ID             := TELEPHONE_TELE_ID_SEQ.nextval;
            v_TeleRec.TELE_COUNTRY_ACC    := null;
            v_TeleRec.TELE_AREA_CODE      := null;
            v_TeleRec.TELE_NUMBER         := p_Fax;
            v_TeleRec.TELE_EXT            := null;
            v_TeleRec.TELE_EFF_DATE_START := sysdate;
            v_TeleRec.TELE_EFF_DATE_END   := null;
--            v_TeleRec.TELE_EDIT_USER_ID   := p_UserName;
            v_TeleRec.TELE_EDIT_USER_ID   := user;
            v_TeleRec.TELE_EDIT_DATE      := sysdate;

            -- Insert record
            v_TraceMsg := 'Insert fax record';
            insert into TELEPHONE values v_TeleRec returning rowid into g_Rowid;
            v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
            PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                             p_TableOwner    => 'RMMISPODS',
                                                             p_TableName     => 'TELEPHONE',
                                                             p_RowId         => g_Rowid);
         when others then
            raise;
      end;
   
         -- Fax assignment record
         -- Retrieve the telephone type foreign key
         v_TraceMsg := 'Get FAX telephone type';
         with curr_rec as
         (select ttyp_code                as curr_code,
                 max(ttyp_eff_start_date) as curr_eff_date
          from REF_TELEPHONE_TYPE
          where ttyp_eff_start_date <= sysdate
          and   ttyp_code            = 'FAX'
          group by ttyp_code
         )
         select ttyp_id_pk
         into v_PtelRec.PTEL_TELE_TTYP_FK
         from REF_TELEPHONE_TYPE
         join curr_rec
             on ttyp_code           = curr_code
            and ttyp_eff_start_date = curr_eff_date;
   
         v_TraceMsg := 'Load FAX record';
         v_PtelRec.PTEL_PRVD_FK          := p_Provider_Id;
         v_PtelRec.PTEL_TELE_FK          := v_TeleRec.TELE_ID;
         v_PtelRec.PTEL_ASSGN_START_DATE := sysdate;
         v_PtelRec.PTEL_ASSGN_END_DATE   := null;
--         v_PtelRec.PTEL_EDIT_USER_ID     := p_UserName;
         v_PtelRec.PTEL_EDIT_USER_ID     := user;
         v_PtelRec.PTEL_EDIT_DATE        := sysdate;
   
         -- Insert record
         v_TraceMsg := 'Insert FAX record';
         insert into PROVIDER_TELEPHONE values v_PtelRec returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'PROVIDER_TELEPHONE',
                                                          p_RowId         => g_Rowid);
      end if;

      if length(nvl(p_EmailAddr, '')) > 0
      then
      begin
         select emal_id
         into v_EmailRec.EMAL_ID
         from EMAIL
         where emal_address = p_EmailAddr;

      exception
         when no_data_found then
            -- Load email record
            v_TraceMsg := 'P_INSERT_ADDRESS: Load email record';
            v_EmailRec.EMAL_ID           := EMAIL_EMAL_ID_SEQ.nextval;
            v_EmailRec.EMAL_ADDRESS      := p_EmailAddr;
--            v_EmailRec.EMAL_EDIT_USER_ID := p_UserName;
            v_EmailRec.EMAL_EDIT_USER_ID := user;
            v_EmailRec.EMAL_EDIT_DATE    := sysdate;

            -- Insert record
            v_TraceMsg := 'P_INSERT_ADDRESS: Insert Email record';
            insert into EMAIL values v_EmailRec returning rowid into g_Rowid;
            v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
            PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                             p_TableOwner    => 'RMMISPODS',
                                                             p_TableName     => 'EMAIL',
                                                             p_RowId         => g_Rowid);
         when others then
            raise;
      end;

         v_TraceMsg := 'P_INSERT_ADDRESS: Load provider email record';
         v_PemalRec.PEML_PRVD_FK          := p_Provider_Id;
         v_PemalRec.PEML_EMAL_FK          := v_EmailRec.EMAL_ID;
         v_PemalRec.PEML_ASSGN_START_DATE := sysdate;
         v_PemalRec.PEML_ASSGN_END_DATE   := null;
--         v_PemalRec.PEML_EDIT_USER_ID     := p_UserName;
         v_PemalRec.PEML_EDIT_USER_ID     := user;
         v_PemalRec.PEML_EDIT_DATE        := sysdate;

         -- Retrieve the telephone type foreign key
         v_TraceMsg := 'P_INSERT_ADDRESS: Get email type';
         with curr_rec as
         (select etyp_code                as curr_code,
                 max(etyp_eff_start_date) as curr_eff_date
          from REF_EMAIL_TYPE
          where etyp_eff_start_date <= sysdate
          and   etyp_code            = 'BUS'
          group by etyp_code
         )
         select etyp_id_pk
         into v_PemalRec.PEML_EMAL_ETYP_FK
         from REF_EMAIL_TYPE
         join curr_rec
             on etyp_code           = curr_code
            and etyp_eff_start_date = curr_eff_date;

         -- Insert record
         v_TraceMsg := 'P_INSERT_ADDRESS: Insert Email record';
         insert into PROVIDER_EMAIL values v_PemalRec returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                          p_TableOwner    => 'RMMISPODS',
                                                          p_TableName     => 'PROVIDER_EMAIL',
                                                          p_RowId         => g_Rowid);
      end if;

      -- Provider Site information
      v_TraceMsg := 'Load Provider site record';
      vPsiteRec.SITE_ID               := PROVIDER_SITE_SITE_ID_SEQ.nextval;
      vPsiteRec.SITE_PRVD_FK          := p_Provider_Id;
      vPsiteRec.SITE_NUMBER           := null;
      vPsiteRec.SITE_NAME             := p_Practice_Name;
      vPsiteRec.SITE_ADDR_TYPE        := null;
      vPsiteRec.SITE_BORDER_STATE_IND := null;
      vPsiteRec.SITE_CBSA_CODE        := null;
      vPsiteRec.SITE_TEACHING_IND     := null;
--      vPsiteRec.SITE_EDIT_USER_ID     := p_UserName;
      vPsiteRec.SITE_EDIT_USER_ID     := user;
      vPsiteRec.SITE_EDIT_DATE        := sysdate;

      begin
         v_TraceMsg := 'Insert Provider site record';
         insert into PROVIDER_SITE values vPsiteRec returning rowid into g_Rowid;
         v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
         PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER_SITE',
                                                       p_RowId         => g_Rowid);

      exception
         when DUP_VAL_ON_INDEX then
            -- If the provider site already exists, then skip
            null;
         when others then
            raise;
      end;

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg);
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg);
         raise;
   end P_INSERT_ADDRESS;

   procedure P_INSERT_CERTIFICATION(p_Provider_Id     in number,
                                    p_CertType        in varchar2,
                                    p_CertEntity      in varchar2,
                                    p_CertNumber      in varchar2,
                                    p_EffectiveDate   in varchar2,
                                    p_ExpirationDate  in varchar2,
                                    p_CertDocId       in varchar2,
                                    p_CertDocFileName in varchar2,
                                    p_TransactionId   in varchar2,
                                    p_SuccessInd     out varchar2)
   is
      v_CtypRec  CERT_TYPE_ISSUING_ENTITY_PROV%rowtype;
      v_CliaRec  CLIA_CERT_SPECIALTY_SITE%rowtype;
      v_CcrtRec  CLIA_CERTIFICATION%rowtype;
      v_LicnRec  LICENSE_PROVIDER%rowtype;
      v_TraceMsg varchar2(155);

   begin
      -- Process provider certification
      -- Get certification type primary key
      v_TraceMsg := 'Get certification type fk';
      with curr_rec as
      (select ctyp_code                as curr_code,
              max(ctyp_eff_start_date) as curr_eff_date
       from REF_CERTIFICATION_TYPE
       where ctyp_eff_start_date <= sysdate
       and   ctyp_code            = p_CertType
       group by ctyp_code
      )
      select ctyp_id_pk
      into v_CtypRec.CERT_CTYP_ID_FK
      from REF_CERTIFICATION_TYPE
      join curr_rec
          on ctyp_code           = curr_code
         and ctyp_eff_start_date = curr_eff_date;

      -- Get certification issuing entity primary key
      v_TraceMsg := 'Get certification issuing entity fk';
      with curr_rec as
      (select cien_code                as curr_code,
              max(cien_eff_start_date) as curr_eff_date
       from REF_CERTIFICATION_ISS_ENTITY
       where cien_eff_start_date <= sysdate
       and   cien_code            = p_CertEntity
       group by cien_code
      )
      select cien_id_pk
      into v_CtypRec.CERT_CIEN_ID_FK
      from REF_CERTIFICATION_ISS_ENTITY
      join curr_rec
          on cien_code           = curr_code
         and cien_eff_start_date = curr_eff_date;

      -- Load provider certification record
      v_TraceMsg := 'Loading provider certification record';
      v_CtypRec.CERT_PRVD_ID_FK    := p_Provider_Id;
      v_CtypRec.CERT_CERT_NUM      := p_CertNumber;
      v_CtypRec.CERT_DOCUMENT_ID   := p_CertDocId;
      v_CtypRec.CERT_DOCUMENT_NAME := p_CertDocFileName;
      v_CtypRec.CERT_BEGIN_DATE    := to_date(p_EffectiveDate, 'mm/dd/yyyy');
      v_CtypRec.CERT_END_DATE      := to_date(p_ExpirationDate, 'mm/dd/yyyy');
      v_CtypRec.CERT_EDIT_USER_ID  := user;
      v_CtypRec.CERT_EDIT_DATE     := sysdate;

      -- Load provider certification record
      v_TraceMsg := 'Loading provider certification record';
      insert into CERT_TYPE_ISSUING_ENTITY_PROV values v_CtypRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'CERT_TYPE_ISSUING_ENTITY_PROV',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_CERTIFICATION;

   procedure P_INSERT_CLIA(p_Provider_Id     in number,
                           p_SiteLoc         in varchar2,
                           p_CLIACertType    in varchar2,
                           p_CertNumber      in varchar2,
                           p_EffectiveDate   in varchar2,
                           p_ExpirationDate  in varchar2,
                           p_UserName        in varchar2,
                           p_UserRole        in varchar2,
                           p_TransactionId   in varchar2,
                           p_SuccessInd     out varchar2)
   is
      v_CliaRec  CLIA_CERT_SPECIALTY_SITE%rowtype;
      v_CcrtRec  CLIA_CERTIFICATION%rowtype;
      v_TraceMsg varchar2(155);

   begin
      -- Process CLIA certification
      -- Retrieve the location information
      v_TraceMsg := 'Retrieving provider site/location data';
      select site_id
      into v_CliaRec.CLIA_SITE_FK
      from provider_site
      where site_prvd_fk = p_Provider_Id
      and   site_name    = p_SiteLoc;

      -- Load provider site CLIA certification record
      v_TraceMsg := 'Loading provider site CLIA certification record';
      v_CliaRec.CLIA_ID_PK        := PTDCLIA_CLIA_ID_PK_SEQ.nextval;
      v_CliaRec.CLIA_NUM          := NULL;
      v_CliaRec.CLIA_PAID_IND     := NULL;
      v_CliaRec.CLIA_EFF_DATE     := to_date(p_EffectiveDate, 'mm/dd/yyyy');
      v_CliaRec.CLIA_EXP_DATE     := to_date(p_ExpirationDate, 'mm/dd/yyyy');
      v_CliaRec.CLIA_EDIT_USER_ID := p_UserName;
      v_CliaRec.CLIA_EDIT_DATE    := sysdate;

      -- Insert provider site CLIA certification record
      v_TraceMsg := 'Inserting provider site CLIA certification record';
      insert into CLIA_CERT_SPECIALTY_SITE values v_CliaRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'CLIA_CERT_SPECIALTY_SITE',
                                                       p_RowId         => g_Rowid);

      -- Load provider site CLIA certification type record
      v_TraceMsg := 'Loading provider site CLIA certification type record';
      v_CcrtRec.CCRT_ID_PK        := PTDCCRT_CCRT_ID_PK_SEQ.nextval;
      v_CcrtRec.CCRT_CLIA_FK      := v_CliaRec.CLIA_ID_PK;
      v_CcrtRec.CCRT_CERT_TYPE    := p_CLIACertType;
      v_CcrtRec.CCRT_CERT_NUM     := p_CertNumber;
      v_CcrtRec.CCRT_FACL_TYPE    := NULL;
      v_CcrtRec.CCRT_EFF_DATE     := to_date(p_EffectiveDate, 'mm/dd/yyyy');
      v_CcrtRec.CCRT_EXP_DATE     := to_date(p_ExpirationDate, 'mm/dd/yyyy');
      v_CcrtRec.CCRT_EDIT_USER_ID := p_UserName;
      v_CcrtRec.CCRT_EDIT_DATE    := sysdate;

      -- Insert provider site CLIA certification type record
      v_TraceMsg := 'Inserting provider site CLIA certification type record';
      insert into CLIA_CERTIFICATION values v_CcrtRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'CLIA_CERTIFICATION',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_CLIA;

   procedure P_INSERT_LICENSE(p_Provider_Id     in number,
                              p_LicenseEntity   in varchar2,
                              p_LicenseNumber   in varchar2,
                              p_LicenseType     in varchar2,
                              p_LicenseCategory in varchar2,
                              p_LicenseDesc     in varchar2,
                              p_State           in varchar2,
                              p_LicnEffDate     in varchar2, -- Format as 'MM/DD/YYYY'
                              p_LicnExpDate     in varchar2, -- Format as 'MM/DD/YYYY'
                              p_LicnDocId       in varchar2,
                              p_LicnDocFileName in varchar2,
                              p_TransactionId   in varchar2,
                              p_SuccessInd     out varchar2)
   is
      v_LicnRec   LICENSE_PROVIDER%rowtype;
      v_TraceMsg  varchar2(155);

   begin
      -- Get the License foreign key
      v_TraceMsg := 'P_INSERT_LICENSE: Retrieve license FK';
      with curr_lcat as
      (select lcat_code                as CURR_LCAT_CODE,
              max(lcat_eff_start_date) as CURR_LCAT_EFF_DT
       from REF_LICENSE_CATEGORY
       where lcat_code = p_LicenseCategory
       group by lcat_code
      ),
      curr_ltyp as
      (select ltyp_code                as CURR_LTYP_CODE,
              max(ltyp_eff_start_date) as CURR_LTYP_EFF_DT
       from REF_LICENSE_TYPE
       where ltyp_code = p_LicenseType
       group by ltyp_code
      ),
      curr_ldes as
      (select ldes_code                as CURR_LDES_CODE,
              max(ldes_eff_start_date) as CURR_LDES_EFF_DT
       from REF_LICENSE_DESCRIPTION
       where ldes_code = p_LicenseDesc
       group by ldes_code
      )
      select licn_id_pk
      into v_LicnRec.LICN_FK
      from REF_LICENSE_DESC_TYPE_CATEGORY a
      join REF_LICENSE_CATEGORY
          on LCAT_ID_PK = LICN_LCAT_ID_FK
      join REF_LICENSE_TYPE
          on LTYP_ID_PK = LICN_LTYP_ID_FK
      join REF_LICENSE_DESCRIPTION
          on LDES_ID_PK = LICN_LDES_ID_FK
      join curr_lcat
          on lcat_code           = CURR_LCAT_CODE
         and lcat_eff_start_date = CURR_LCAT_EFF_DT
      join curr_ltyp
          on ltyp_code           = CURR_LTYP_CODE
         and ltyp_eff_start_date = CURR_LTYP_EFF_DT
      join curr_ldes
          on ldes_code           = CURR_LDES_CODE
         and ldes_eff_start_date = CURR_LDES_EFF_DT
      where LICN_EFF_START_DATE <= sysdate
      and   sysdate between
               LICN_EFF_START_DATE and nvl(LICN_DEACTIVE_DATE, to_date('12312099', 'MMDDYYYY'));

      -- Load the table record
      v_TraceMsg := 'P_INSERT_LICENSE: Load license type record';
      v_LicnRec.LICN_ISS_ENT        := p_LicenseEntity;
      v_LicnRec.LICN_PRVD_FK        := p_Provider_Id;
      v_LicnRec.LICN_NUM            := p_LicenseNumber;
      v_LicnRec.LICN_DOCUMENT_ID    := p_LicnDocId;
      v_LicnRec.LICN_DOCUMENT_NAME  := p_LicnDocFileName;
      v_LicnRec.LICN_BEGIN_DATE     := to_date(p_LicnEffDate, 'mm/dd/yyyy');
      v_LicnRec.LICN_END_DATE       := to_date(p_LicnExpDate, 'mm/dd/yyyy');
      v_LicnRec.LICN_EDIT_USER_ID   := user;
      v_LicnRec.LICN_EDIT_DATE      := sysdate;

      -- Insert license type record
      v_TraceMsg := 'P_INSERT_LICENSE: Inserting license type record';
      insert into LICENSE_PROVIDER values v_LicnRec returning rowid into g_Rowid;

      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'LICENSE_PROVIDER',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_LICENSE;

   procedure P_INSERT_OFFICE_HOURS(p_Provider_Id   in number,
                                   p_SiteLoc       in varchar2,
                                   p_WorkingDay    in varchar2,
                                   p_FromTime      in varchar2,
                                   p_ToTime        in varchar2,
                                   p_Open24hrInd   in varchar2,
                                   p_ClosedInd     in varchar2,
                                   p_TransactionId in varchar2,
                                   p_SuccessInd    out varchar2)
   is
      v_SiteHrsRec SITE_HOURS_OPERATION%rowtype;
      v_SitePk     PROVIDER_SITE.SITE_ID%type;
      v_TraceMsg   varchar2(155);

   begin
      v_TraceMsg := 'Get the provider location primary key';
      select site_id
      into v_SitePk
      from provider_site
      where site_prvd_fk = p_Provider_Id
      and   site_name    = p_SiteLoc;

      v_TraceMsg := 'Load Site Hours of Operation table record';
      v_SiteHrsRec.SHRS_SITE_FK      := v_SitePk;
      v_SiteHrsRec.SHRS_DAY          := p_WorkingDay;
      v_SiteHrsRec.SHRS_START_TIME   := p_FromTime;
      v_SiteHrsRec.SHRS_END_TIME     := p_ToTime;
      v_SiteHrsRec.SHRS_24HR_IND     := p_Open24hrInd;
      v_SiteHrsRec.SHRS_CLOSED_IND   := p_ClosedInd;
      v_SiteHrsRec.SHRS_EDIT_USER_ID := user;
      v_SiteHrsRec.SHRS_EDIT_DATE    := sysdate;

      v_TraceMsg := 'Insert Site Hours of Operation table record';
      insert into SITE_HOURS_OPERATION values v_SiteHrsRec returning rowid into g_Rowid;
      v_TraceMsg := 'Create transaction record for: '||v_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'SITE_HOURS_OPERATION',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when no_data_found then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
      when others then
         dbms_output.put_line(v_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_OFFICE_HOURS;

   procedure P_INSERT_AFFILIATION(p_Provider_Id   in number,
                                  p_AgencyProvId  in varchar2,
                                  p_Npi           in varchar2,
                                  p_OrgnName      in varchar2,
                                  p_TransactionId in varchar2,
                                  p_SuccessInd    out varchar2)
   is
      v_AffilRec PROVIDER_AFFILIATION%rowtype;

   begin
      g_TraceMsg := 'P_INSERT_AFFILIATION: Load table record';
      v_AffilRec.PRAF_PRVD_FK      := p_Provider_Id;
      v_AffilRec.PRAF_AFLT_PRVD_FK := f_Npi_Lookup(p_Npi);
      v_AffilRec.PRAF_TYPE         := null;
      v_AffilRec.PRAF_ID           := null;
      v_AffilRec.PRAF_EFF_DATE     := sysdate;
      v_AffilRec.PRAF_END_DATE     := null;
      v_AffilRec.PRAF_EDIT_USER    := user;
      v_AffilRec.PRAF_EDIT_DATE    := sysdate;

      g_TraceMsg := 'P_INSERT_AFFILIATION: Insert table record';
      insert into PROVIDER_AFFILIATION values v_AffilRec returning rowid into g_Rowid;
      g_TraceMsg := 'Create transaction record for: '||g_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'PROVIDER_AFFILIATION',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when no_data_found then
         dbms_output.put_line(g_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
      when others then
         dbms_output.put_line(g_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_AFFILIATION;

   procedure P_INSERT_OWNERSHIP(p_Provider_Id   in number,
                                p_OwnerTypeCode in varchar2,
                                p_FirstName     in varchar2,
                                p_MiddleName    in varchar2,
                                p_LastName      in varchar2,
                                p_Ssn           in varchar2,
                                p_BirthDate     in varchar2,
                                p_BusName       in varchar2,
                                p_Ein           in varchar2,
                                p_OwnPerct      in varchar2,
                                p_OwnNumShares  in varchar2,
                                p_OwnRelCode    in varchar2,
                                p_TransactionId in varchar2,
                                p_SuccessInd    out varchar2)
   is
      v_PownRec  ENTITY_PROVIDER_OWNERSHIP%rowtype;
      v_EntityId ENTITY.ENTY_PK%type;

   begin
      -- Retrieve the ownership type foreign key
      g_TraceMsg := 'P_INSERT_OWNERSHIP: Get owner type';
      with curr_rec as
      (select ownt_code                as curr_code,
              max(ownt_eff_start_date) as curr_eff_date
       from REF_PROVIDER_OWNERSHIP_TYPE
       where ownt_eff_start_date <= sysdate
       and   ownt_code            = p_OwnerTypeCode
       group by ownt_code
      )
      select ownt_id_pk
      into v_PownRec.POWN_OWNT_FK
      from REF_PROVIDER_OWNERSHIP_TYPE
      join curr_rec
          on ownt_code           = curr_code
         and ownt_eff_start_date = curr_eff_date;

      -- Retrieve the ownership relation foreign key
      g_TraceMsg := 'P_INSERT_OWNERSHIP: Get owner type';
      with curr_rec as
      (select ownr_code                as curr_code,
              max(ownr_eff_start_date) as curr_eff_date
       from REF_OWNERSIP_RELATION
       where ownr_eff_start_date <= sysdate
       and   ownr_code            = p_OwnRelCode
       group by ownr_code
      )
      select ownr_id_pk
      into v_PownRec.POWN_OWNR_FK
      from REF_OWNERSIP_RELATION
      join curr_rec
          on ownr_code           = curr_code
         and ownr_eff_start_date = curr_eff_date;

      g_TraceMsg := 'P_INSERT_OWNERSHIP: Process entity information';
      p_Insert_Entity(p_FirstName        => p_FirstName,
                      p_MiddleName       => p_MiddleName,
                      p_LastName         => p_LastName,
                      p_Suffix           => null,
                      p_OrgnName         => null,
                      p_DBA_Name         => null,
                      p_Years_DBA        => null,
                      p_Former_DBA_Name  => null,
                      p_Years_Former_DBA => null,
                      p_TIN              => null,
                      p_EIN              => p_EIN,
                      p_SSN              => p_SSN,
                      p_Gender           => null,
                      p_DOB              => null,
                      p_UserName         => user,
                      p_UserRole         => null,
                      p_TransactionId    => p_TransactionId,
                      p_EntityId         => v_EntityId);

      g_TraceMsg := 'P_INSERT_OWNERSHIP: Load table record';
      v_PownRec.POWN_PRVD_FK      := p_Provider_Id;
      v_PownRec.POWN_ENTY_FK      := v_EntityId;
      v_PownRec.POWN_BEGIN_DATE   := sysdate;
      v_PownRec.POWN_END_DATE     := null;
      v_PownRec.POWN_PCT          := to_number(p_OwnPerct);
      v_PownRec.POWN_NUM_SHARES   := to_number(p_OwnNumShares);
      v_PownRec.POWN_EDIT_USER_ID := user;
      v_PownRec.POWN_EDIT_DATE    := sysdate;

      g_TraceMsg := 'P_INSERT_OWNERSHIP: Insert table record';
      insert into ENTITY_PROVIDER_OWNERSHIP values v_PownRec returning rowid into g_Rowid;
      g_TraceMsg := 'Create transaction record for: '||g_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'ENTITY_PROVIDER_OWNERSHIP',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when others then
         dbms_output.put_line(g_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_OWNERSHIP;

   procedure P_INSERT_MANAGE_REL(p_Provider_Id   in number,
                                 p_OrgnName      in varchar2,
                                 p_Ein           in varchar2,
                                 p_FirstName     in varchar2,
                                 p_MiddleName    in varchar2,
                                 p_LastName      in varchar2,
                                 p_Suffix        in varchar2,
                                 p_Ssn           in varchar2,
                                 p_BirthDate     in varchar2,
                                 p_BusRelCode    in varchar2,
                                 p_FamRelCode    in varchar2,
                                 p_OtherExplain  in varchar2,
                                 p_TransactionId in varchar2,
                                 p_SuccessInd    out varchar2)
   is
      v_PrelRec  ENTITY_PROVIDER_RELATION%rowtype;
      v_EntityId ENTITY.ENTY_PK%type;

   begin
      -- Retrieve the familial relation foreign key
      g_TraceMsg := 'P_INSERT_MANAGE_REL: Get family relation';
      with curr_rec as
      (select famr_code                as curr_code,
              max(famr_eff_start_date) as curr_eff_date
       from REF_FAMILY_RELATION
       where famr_eff_start_date <= sysdate
       and   famr_code            = p_FamRelCode
       group by famr_code
      )
      select famr_id_pk
      into v_PrelRec.PREL_FAMR_FK
      from REF_FAMILY_RELATION
      join curr_rec
          on famr_code           = curr_code
         and famr_eff_start_date = curr_eff_date;

      -- Retrieve the business relation foreign key
      g_TraceMsg := 'P_INSERT_MANAGE_REL: Get business relation';
      with curr_rec as
      (select bsrl_code                as curr_code,
              max(bsrl_eff_start_date) as curr_eff_date
       from REF_PROVIDER_BUSINESS_RELATION
       where bsrl_eff_start_date <= sysdate
       and   bsrl_code            = p_BusRelCode
       group by bsrl_code
      )
      select bsrl_id_pk
      into v_PrelRec.PREL_BSRL_FK
      from REF_PROVIDER_BUSINESS_RELATION
      join curr_rec
          on bsrl_code           = curr_code
         and bsrl_eff_start_date = curr_eff_date;

      g_TraceMsg := 'P_INSERT_MANAGE_REL: Process entity information';
      p_Insert_Entity(p_FirstName        => p_FirstName,
                      p_MiddleName       => p_MiddleName,
                      p_LastName         => p_LastName,
                      p_Suffix           => p_Suffix,
                      p_OrgnName         => p_OrgnName,
                      p_DBA_Name         => null,
                      p_Years_DBA        => null,
                      p_Former_DBA_Name  => null,
                      p_Years_Former_DBA => null,
                      p_TIN              => null,
                      p_EIN              => p_Ein,
                      p_SSN              => p_Ssn,
                      p_Gender           => null,
                      p_DOB              => p_BirthDate,
                      p_UserName         => user,
                      p_UserRole         => null,
                      p_TransactionId    => p_TransactionId,
                      p_EntityId         => v_EntityId);

      g_TraceMsg := 'P_INSERT_MANAGE_REL: Load table record';
      v_PrelRec.PREL_PRVD_FK      := p_Provider_Id;
      v_PrelRec.PREL_ENTY_FK      := v_EntityId;
      v_PrelRec.PREL_OTHER_EXPL   := p_OtherExplain;
      v_PrelRec.PREL_BEGIN_DATE   := sysdate;
      v_PrelRec.PREL_END_DATE     := null;
      v_PrelRec.PREL_EDIT_USER_ID := user;
      v_PrelRec.PREL_EDIT_DATE    := sysdate;

      g_TraceMsg := 'P_INSERT_MANAGE_REL: Insert table record';
      insert into ENTITY_PROVIDER_RELATION values v_PrelRec returning rowid into g_Rowid;
      g_TraceMsg := 'Create transaction record for: '||g_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'ENTITY_PROVIDER_RELATION',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when others then
         dbms_output.put_line(g_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_MANAGE_REL;

   procedure P_INSERT_PROVIDER_CONTACT(p_Provider_Id   in number,
                                       p_SiteLoc       in varchar2,
                                       p_ContactName   in varchar2,
                                       p_EmailAddr     in varchar2,
                                       p_TransactionId in varchar2,
                                       p_UserName      in varchar2,
                                       p_UserRole      in varchar2,
                                       p_SuccessInd    out varchar2)
   is
      v_AdminRec ENTITY_PROV_SITE_ADMIN_CONT%rowtype;
      v_EntityId ENTITY.ENTY_PK%type;

   begin
      -- If a site location value is passed in, look up the site PK
      if length(nvl(p_SiteLoc, '')) > 0
      then
      begin
         g_TraceMsg := 'P_INSERT_PROVIDER_CONTACT: Retrieve site location';
         select SITE_ID
         into v_AdminRec.ADCT_SITE_FK
         from PROVIDER_SITE
         where SITE_NAME = p_SiteLoc
         and   SITE_PRVD_FK = p_Provider_Id;
      exception
         when no_data_found then
            v_AdminRec.ADCT_SITE_FK := null;
         when others then
         raise;
      end;
      end if;

      g_TraceMsg := 'P_INSERT_PROVIDER_CONTACT: Load table record';
      v_AdminRec.ADCT_PRVD_FK      := p_Provider_Id;
      v_AdminRec.ADCT_ENTY_FK      := null;
      v_AdminRec.ADCT_START_DATE   := sysdate;
      v_AdminRec.ADCT_END_DATE     := null;
      v_AdminRec.ADCT_EDIT_USER_ID := p_UserName;
      v_AdminRec.ADCT_EDIT_DATE    := sysdate;

      g_TraceMsg := 'P_INSERT_PROVIDER_CONTACT: Insert table record';
      insert into ENTITY_PROV_SITE_ADMIN_CONT values v_AdminRec returning rowid into g_Rowid;
      g_TraceMsg := 'Create transaction record for: '||g_TraceMsg;
      PKG_TRANSACTION_CONTROL.P_ADD_TRANSACTION_RECORD(p_TransactionId => p_TransactionId,
                                                       p_TableOwner    => 'RMMISPODS',
                                                       p_TableName     => 'ENTITY_PROV_SITE_ADMIN_CONT',
                                                       p_RowId         => g_Rowid);

      p_SuccessInd := 'Y';

   exception
      when others then
         dbms_output.put_line(g_TraceMsg||'; '||sqlerrm);
         p_SuccessInd := 'N';
         raise;
   end P_INSERT_PROVIDER_CONTACT;

end PKG_PROVIDER;
/

show errors;

create or replace public synonym PKG_PROVIDER for RMMISPODS.PKG_PROVIDER
/

grant execute on PKG_PROVIDER to RMMIS_APPLICATION_USER
/

