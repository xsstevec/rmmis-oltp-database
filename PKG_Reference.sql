--
-- Example query to use pipelined function
-- select *
-- from table(odsrefd.PKG_REFERENCE_DATA.F_GET_REF_CODE_TYPE_LIST(sysdate))
-- /
--

ALTER SESSION SET CURRENT_SCHEMA = rmmispods;

create or replace
package PKG_REFERENCE_DATA as
   -----------------------------------------------------------------------
   --
   -----------------------------------------------------------------------

--   function F_GET_REF_CODE_TYPE_LIST(p_EffDate date default sysdate)
--   return t_RefCodeTypeTab pipelined;
--
   procedure P_GET_REF_CODE_TYPE_REFCUR(p_RefCursor out sys_refcursor);

   procedure P_GET_ALL_TXMY_CODE_RC(p_InactiveList in varchar2,
                                    p_InternalList in varchar2,
                                    p_ProviderType in varchar2,
                                    p_GroupName    in varchar2,
                                    p_CodeEffDate  in date,
                                    p_RefCursor    out sys_refcursor);

   procedure P_UPDATE_TXMY_RISK_LEVEL(p_Taxonomy_Code in varchar2,
                                      p_ProviderType  in varchar2,
                                      p_Specialty     in varchar2,
                                      p_RiskLevel     in varchar2,
                                      p_ModifiedBy    in varchar2,
                                      pSuccessInd     out varchar2);

   procedure P_TXMY_FETCHBY_ENROLL_TYPE_RC(p_EnrollmentType in varchar2,
                                           p_RefCursor      out sys_refcursor);

----------------------------------------------------------------------------------------
-- These taxonomy procedures operate on the taxonomy code table, only
----------------------------------------------------------------------------------------
   procedure P_UPDATE_TXMY_CODE(p_Taxonomy_Code in varchar2,
                                p_GroupName     in varchar2,
                                p_ProvType      in varchar2,
                                p_ProvSpec      in varchar2,
                                p_InactiveInd   in varchar2,
                                p_InternalInd   in varchar2,
                                p_CodeEffDate   in date,
                                p_DeactiveDate  in date,
                                p_Definition    in varchar2,
                                p_User          in varchar2,
                                p_Notes         in varchar2,
                                pSuccessInd     out varchar2);

   procedure P_CREATE_TXMY_CODE(p_Taxonomy_Code    in varchar2,
                                p_GroupName        in varchar2,
                                p_ProviderType     in varchar2,
                                p_Specialty        in varchar2,
                                p_InactiveInd      in varchar2,
                                p_InternalInd      in varchar2,
                                p_DateActiveFrom   in date,
                                p_DateInactiveFrom in date,
                                p_Definition       in varchar2,
                                p_Notes            in varchar2,
                                p_ModifiedBy       in varchar2,
                                pSuccessInd        out varchar2);
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- These taxonomy procedures operate on the taxonomy association table, only
----------------------------------------------------------------------------------------
   procedure P_CREATE_TXMY_ASSOCIATION(p_TAXONOMY_CODE   in varchar2,
                                       p_ENROLLMENT_TYPE in varchar2,
                                       p_PROVIDER_TYPE   in varchar2,
                                       p_SPECIALTY       in varchar2,
                                       p_RISK_LEVEL      in varchar2,
                                       p_IS_TYPICAL      in varchar2,
                                       p_EFFECTIVE_DATE  in varchar2,
                                       p_END_DATE        in varchar2,
                                       p_USER_ID         in varchar2,
                                       pSuccessInd       out varchar2);

   procedure P_GET_TXMY_ASSOCIATION(p_TAXONOMY_CODE   in out varchar2,
                                    p_ENROLLMENT_TYPE in out varchar2,
                                    p_PROVIDER_TYPE   in out varchar2,
                                    p_SPECIALTY       in out varchar2,
                                    p_RefCursor       out sys_refcursor);

   procedure P_UPDATE_TXMY_ASSOCIATION(p_ASSOCIATION_ID  in out number,
                                       p_RISK_LEVEL      in varchar2,
                                       p_IS_TYPICAL      in varchar2,
                                       p_EFFECTIVE_DATE  in varchar2,
                                       p_ACTIVE_DATE     in varchar2,
                                       p_USER_ID         in varchar2,
                                       pSuccessInd       out varchar2);

   procedure P_DELETE_TXMY_ASSOCIATION(p_ASSOCIATION_ID in out number,
                                       p_DELETE_DATE    in varchar2,
                                       p_USER_ID        in varchar2,
                                       pSuccessInd      out varchar2);                 
----------------------------------------------------------------------------------------

   procedure P_GET_REF_CODE_BY_TYPE_RC(p_RefTableName in varchar2,
                                       p_RefCursor    out sys_refcursor);

   procedure P_VERIFY_IN_DMF(p_Ssn       in varchar2,
                             p_RefCursor out sys_refcursor);

   procedure P_VERIFY_IN_DMF_Name(p_first_name in  varchar2,
                                  p_last_name  in  varchar2,
                                  p_RefCursor  out sys_refcursor);

   procedure P_VERIFY_IN_DEA_REGISTRATION(p_registration in  varchar2,
                                          p_RefCursor    out sys_refcursor);

   procedure P_GET_LICENSE_RC(p_LicenseType     in  varchar2,
                              p_LicenseCategory in  varchar2,
                              p_RefCursor       out sys_refcursor);

   procedure P_GET_CERT_TYPE_RC(p_RefCursor out sys_refcursor);

   procedure P_GET_CERT_ISS_ENT_RC(p_RefCursor out sys_refcursor);

   function F_GET_CITY_STATE_LIST(p_StateCode in varchar2)
   return sys_refcursor;

   function F_IS_VALID_CITY(p_CityName   varchar2,
                             p_StateCode varchar2)
   return varchar2;

   function F_IS_VALID_MSA_CITY(p_CityName  varchar2,
                                p_StateCode varchar2)
   return varchar2;

end PKG_REFERENCE_DATA;
/

show errors;

create or replace package body PKG_REFERENCE_DATA as
   ----------------------------------------------------------------
   -- Private global variables
   ----------------------------------------------------------------
      g_CodeType     varchar2(10);
      g_CodeTypeDesc varchar2(100);
      g_TableName    varchar2(100);
      g_Error        varchar2(155) := '';
      g_RowCnt       number := 0;

      x_CustomError exception;
      PRAGMA EXCEPTION_INIT(x_CustomError, -20001 );

   ----------------------------------------------------------------
   -- Private procedures/functions
   ----------------------------------------------------------------
   -- Retrieve the Taxonomy Code ID
   function f_Get_Txmy_Code_Id(p_TxmyCode    varchar2,
                               p_CodeEffDate date)
   return number
   is
      v_IdPk number;

   begin
      with curr_rec as
      (select TXMY_CODE                as curr_code,
              max(TXMY_EFF_START_DATE) as curr_eff_date
       from REF_TAXONOMY
       where TXMY_CODE = p_TxmyCode
       and   nvl(p_CodeEffDate, sysdate) between
             TXMY_EFF_START_DATE and
             nvl(TXMY_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by TXMY_CODE
      )
      select TXMY_ID_PK
      into v_IdPk
      from REF_TAXONOMY
      join CURR_REC
          on CURR_CODE     = TXMY_CODE
         and CURR_EFF_DATE = TXMY_EFF_START_DATE;

      return v_IdPk;

   exception
      when no_data_found then
         return NULL;
      when others then
         return NULL;
   end f_Get_Txmy_Code_Id;

   -- Retrieve the Provider Type ID based upon the Provider Type Description
   function f_Get_Prov_Type_Id(p_ProvType    varchar2,
                               p_CodeEffDate date)
   return number
   is
      v_IdPk number;

   begin
      with curr_rec as
      (select PTYP_CODE                as curr_code,
              max(PTYP_EFF_START_DATE) as curr_eff_date
       from REF_PROVIDER_TYPE
       where PTYP_CODE_DESC = p_ProvType
       and   nvl(p_CodeEffDate, sysdate) between
             PTYP_EFF_START_DATE and
             nvl(PTYP_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by PTYP_CODE
      )
      select PTYP_ID_PK
      into v_IdPk
      from REF_PROVIDER_TYPE
      join CURR_REC
          on CURR_CODE     = PTYP_CODE
         and CURR_EFF_DATE = PTYP_EFF_START_DATE;

      return v_IdPk;

   exception
      when no_data_found then
         return NULL;
      when others then
         return NULL;
   end f_Get_Prov_Type_Id;

   -- Retrieve the Provider Specialty ID based upon the Provider Specialty Description
   function f_Get_Prov_Spec_Id(p_ProvSpec    varchar2,
                               p_CodeEffDate date)
   return number
   is
      v_IdPk number;

   begin
      with curr_rec as
      (select SPEC_CODE                as curr_code,
              max(SPEC_EFF_START_DATE) as curr_eff_date
       from REF_PROVIDER_SPECIALTY
       where SPEC_CODE_DESC = p_ProvSpec
       and   nvl(p_CodeEffDate, sysdate) between
             SPEC_EFF_START_DATE and
             nvl(SPEC_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by SPEC_CODE
      )
      select SPEC_ID_PK
      into v_IdPk
      from REF_PROVIDER_SPECIALTY
      join CURR_REC
          on CURR_CODE     = SPEC_CODE
         and CURR_EFF_DATE = SPEC_EFF_START_DATE;

      return v_IdPk;

   exception
      when no_data_found then
         return NULL;
      when others then
         return NULL;
   end f_Get_Prov_Spec_Id;

   -- Retrieve the Provider Enrollment Type ID based upon the description
   function f_Get_Enrl_Type_Id(p_EnrlType    varchar2,
                               p_CodeEffDate date)
   return number
   is
      v_IdPk number;

   begin
      with curr_rec as
      (select ENRL_CODE                as curr_code,
              max(ENRL_EFF_START_DATE) as curr_eff_date
       from REF_ENROLLMENT_TYPE
       where ENRL_CODE = p_EnrlType
       and   nvl(p_CodeEffDate, sysdate) between
             ENRL_EFF_START_DATE and
             nvl(ENRL_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by ENRL_CODE
      )
      select ENRL_ID_PK
      into v_IdPk
      from REF_ENROLLMENT_TYPE
      join CURR_REC
          on CURR_CODE     = ENRL_CODE
         and CURR_EFF_DATE = ENRL_EFF_START_DATE;

      return v_IdPk;

   exception
      when no_data_found then
         return NULL;
      when others then
         return NULL;
   end f_Get_Enrl_Type_Id;

   ----------------------------------------------------------------
   -- Public procedures/functions
   ----------------------------------------------------------------

   ----------------------------------------------------------------
   procedure P_GET_REF_CODE_TYPE_REFCUR(p_RefCursor out sys_refcursor)
   as

   begin
      /* Get a list of reference tables by opening this Reference cursor */
      open p_RefCursor for
      select comments as "label",
             table_name as "value"
      from all_tab_comments
      where owner = 'RMMISPODS'
      and   table_name like 'REF_%'
      order by table_name;

      return;
   end P_GET_REF_CODE_TYPE_REFCUR;

   ----------------------------------------------------------------
   procedure P_GET_ALL_TXMY_CODE_RC(p_InactiveList in varchar2,
                                    p_InternalList in varchar2,
                                    p_ProviderType in varchar2,
                                    p_GroupName    in varchar2,
                                    p_CodeEffDate  in date,
                                    p_RefCursor    out sys_refcursor)
   as
      v_InactiveInd REF_TAXONOMY.txmy_inactive_ind%type;
      v_InternalInd REF_TAXONOMY.txmy_internal_ind%type;

   begin
      -- If these indicators are not set to 'Y', then set them to NULL
      if upper(p_InactiveList) <> 'Y'
      then
         v_InactiveInd := NULL;
      end if;

      if upper(p_InternalList) <> 'Y'
      then
         v_InternalInd := NULL;
      end if;

      /* Open the reference cursor */
      open p_RefCursor for
      with curr_rec as
      (select TXMY_CODE                as curr_code,
              max(TXMY_EFF_START_DATE) as curr_eff_date
       from REF_TAXONOMY
       where nvl(sysdate, sysdate) between
             TXMY_EFF_START_DATE and
             nvl(TXMY_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by TXMY_CODE
      )
      select distinct
             NVL(TXMY_CODE,' ')                                 AS "taxonomyCode",
             NVL(TXMY_GROUP_NAME,' ')                           AS "groupName",
             NVL((select PTYP_CODE_DESC
                  from REF_PROVIDER_TYPE
                  where PTYP_ID_PK = TCWK_PTYP_FK),' ')   AS "providerType",
             NVL((select SPEC_CODE_DESC
                  from REF_PROVIDER_SPECIALTY
                  where SPEC_ID_PK = TCWK_SPEC_FK),' ')   AS "specialty",
             NVL(TXMY_INACTIVE_IND,' ')                         AS "isInactive",
             NVL(TXMY_INTERNAL_IND,' ')                         AS "isInternal",
             NVL(TO_CHAR(TXMY_EFF_START_DATE,'MM/DD/YYYY'),' ') AS "dateActiveFrom",
             NVL(TO_CHAR(TXMY_DEACTIVE_DATE,'MM/DD/YYYY'),' ')  AS "dateInactiveFrom",
             NVL(TXMY_CODE_DESC,' ')                            AS "definition",
             NVL(TCWK_RISK_LVL,' ')                             AS "riskLevel"
      from REF_TAXONOMY
      join CURR_REC
          on CURR_CODE     = TXMY_CODE
         and CURR_EFF_DATE = TXMY_EFF_START_DATE
      join REF_PRV_SPC_TYP_TAX_ENRL_STAT
          on TCWK_TXMY_FK = TXMY_ID_PK
      where nvl(TXMY_GROUP_NAME,   'NONE') = nvl(p_GroupName, nvl(TXMY_GROUP_NAME, 'NONE'))
      and   nvl(TXMY_INACTIVE_IND, 'N')    = nvl(v_InactiveInd, nvl(TXMY_INACTIVE_IND, 'N'))
      and   nvl(TXMY_INTERNAL_IND, 'N')    = nvl(v_InternalInd, nvl(TXMY_INTERNAL_IND, 'N'))
      order by 1;

      return;

   exception
   when no_data_found then
      raise;
   when others then
      raise;
   end P_GET_ALL_TXMY_CODE_RC;

   ----------------------------------------------------------------
   procedure P_UPDATE_TXMY_CODE(p_Taxonomy_Code in varchar2,
                                p_GroupName     in varchar2,
                                p_ProvType      in varchar2,
                                p_ProvSpec      in varchar2,
                                p_InactiveInd   in varchar2,
                                p_InternalInd   in varchar2,
                                p_CodeEffDate   in date,
                                p_DeactiveDate  in date,
                                p_Definition    in varchar2,
                                p_User          in varchar2,
                                p_Notes         in varchar2,
                                pSuccessInd     out varchar2)
   as
      v_TxmyRec    REF_TAXONOMY%rowtype;
      v_NewEffDate date;

   begin
      -- Validate input
      if length(p_Taxonomy_Code) < 10
      then
         pSuccessInd := 'N';
         raise_application_error( -20001, 'The Taxonomy code is not a valid length: ->'||p_Taxonomy_Code||';');
      end if;

      -- Retrieve the most recent, current record for the code in question
      with curr_rec as
      (select TXMY_CODE                as curr_code,
              max(TXMY_EFF_START_DATE) as curr_eff_date
       from REF_TAXONOMY
       where TXMY_CODE = p_Taxonomy_Code
       and   nvl(p_CodeEffDate, sysdate) between
             trunc(TXMY_EFF_START_DATE) and
             nvl(TXMY_DEACTIVE_DATE, to_date('12312199', 'MMDDYYYY'))
       group by TXMY_CODE
      )
      select REF_TAXONOMY.*
      into v_TxmyRec
      from REF_TAXONOMY
      join CURR_REC
          on CURR_CODE     = TXMY_CODE
         and CURR_EFF_DATE = TXMY_EFF_START_DATE
      ;

      -- If the updated code has the same effective date as the existing code, then add
      -- 1 second to the effective date of the exiting code to use for the new
      -- effective date. Typically, the only time this will be an issue is when
      -- the new code version effective date or the existing code effective date
      -- does not have a timestamp with the date (ie both dates are the same and
      -- at midnight).
      if p_CodeEffDate = trunc(v_TxmyRec.TXMY_EFF_START_DATE)
      then
         v_NewEffDate := v_TxmyRec.TXMY_EFF_START_DATE + (1 / (24 * 60 * 60));
      else
         v_NewEffDate := p_CodeEffDate;
      end if;

      -- Set the new Taxonomy table values
      v_TxmyRec.TXMY_ID_PK          := RTDTXMY_TXMY_ID_PK_SEQ.nextval;
      v_TxmyRec.TXMY_CODE_DESC      := p_Definition;
      v_TxmyRec.TXMY_GROUP_NAME     := p_GroupName;
      v_TxmyRec.TXMY_INACTIVE_IND   := p_InactiveInd;
      v_TxmyRec.TXMY_INTERNAL_IND   := p_InternalInd;
      v_TxmyRec.TXMY_EFF_START_DATE := v_NewEffDate;
      v_TxmyRec.TXMY_DEACTIVE_DATE  := p_DeactiveDate;
      v_TxmyRec.TXMY_EDIT_USER_ID   := p_User;
      v_TxmyRec.TXMY_EDIT_DATE      := sysdate;

      -- Insert the new replacement record
      insert into REF_TAXONOMY values v_TxmyRec;
      pSuccessInd := 'Y';

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when no_data_found then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_UPDATE_TXMY_CODE;

   ----------------------------------------------------------------
   procedure P_CREATE_TXMY_CODE(p_Taxonomy_Code    in varchar2,
                                p_GroupName        in varchar2,
                                p_ProviderType     in varchar2,
                                p_Specialty        in varchar2,
                                p_InactiveInd      in varchar2,
                                p_InternalInd      in varchar2,
                                p_DateActiveFrom   in date,
                                p_DateInactiveFrom in date,
                                p_Definition       in varchar2,
                                p_Notes            in varchar2,
                                p_ModifiedBy       in varchar2,
                                pSuccessInd        out varchar2)
   as
      v_TxmyRec REF_TAXONOMY%rowtype;
      v_TcwkRec REF_PRV_SPC_TYP_TAX_ENRL_STAT%rowtype;

   begin
      -- Validate input
      if length(p_Taxonomy_Code) < 10
      then
         pSuccessInd := 'N';
         raise_application_error( -20001, 'The Taxonomy code is not a valid length: ->'||p_Taxonomy_Code||';');
      end if;

      -- Set the new values
      v_TxmyRec.TXMY_ID_PK          := RTDTXMY_TXMY_ID_PK_SEQ.nextval;
      v_TxmyRec.TXMY_CODE           := p_Taxonomy_Code;
      v_TxmyRec.TXMY_CODE_DESC      := p_Definition;
      v_TxmyRec.TXMY_GROUP_NAME     := p_GroupName;
      v_TxmyRec.TXMY_INACTIVE_IND   := p_InactiveInd;
      v_TxmyRec.TXMY_INTERNAL_IND   := p_InternalInd;
      v_TxmyRec.TXMY_EFF_START_DATE := p_DateActiveFrom;
      v_TxmyRec.TXMY_DEACTIVE_DATE  := p_DateInactiveFrom;
      v_TxmyRec.TXMY_EDIT_USER_ID   := p_ModifiedBy;
      v_TxmyRec.TXMY_EDIT_DATE      := sysdate;

      -- Insert the new record
      insert into REF_TAXONOMY values v_TxmyRec;
      pSuccessInd := 'Y';

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_CREATE_TXMY_CODE;

   ----------------------------------------------------------------
   procedure P_CREATE_TXMY_ASSOCIATION(p_TAXONOMY_CODE   in varchar2,
                                       p_ENROLLMENT_TYPE in varchar2,
                                       p_PROVIDER_TYPE   in varchar2,
                                       p_SPECIALTY       in varchar2,
                                       p_RISK_LEVEL      in varchar2,
                                       p_IS_TYPICAL      in varchar2,
                                       p_EFFECTIVE_DATE  in varchar2,
                                       p_END_DATE        in varchar2,
                                       p_USER_ID         in varchar2,
                                       pSuccessInd       out varchar2)
   as
      v_TcwkRec REF_PRV_SPC_TYP_TAX_ENRL_STAT%rowtype;
      v_EffDate date;
      v_EndDate date;

   begin
      pSuccessInd := 'N';
      v_EffDate := to_date(p_EFFECTIVE_DATE, 'MM/DD/YYYY');

      if length(p_END_DATE) = 0
      then
         v_TcwkRec.TCWK_DEACTIVE_DATE := NULL;
      else
         v_TcwkRec.TCWK_DEACTIVE_DATE := to_date(p_END_DATE, 'MM/DD/YYYY');
      end if;

      -- Set the new Taxonomy Crosswalk values
      v_TcwkRec.TCWK_ID_PK          := RTDTCWK_TCWK_ID_PK_SEQ.nextval;
      v_TcwkRec.TCWK_TXMY_FK        := f_Get_Txmy_Code_Id(p_TAXONOMY_CODE,
                                                          v_EffDate);
      v_TcwkRec.TCWK_PTYP_FK        := f_Get_Prov_Type_Id(p_PROVIDER_TYPE,
                                                          v_EffDate);
      v_TcwkRec.TCWK_SPEC_FK        := f_Get_Prov_Spec_Id(p_SPECIALTY,
                                                          v_EffDate);
      v_TcwkRec.TCWK_ENRL_FK        := f_Get_Enrl_Type_Id(p_ENROLLMENT_TYPE,
                                                          v_EffDate);
      v_TcwkRec.TCWK_HIPAA_TYPE     := NULL;
      v_TcwkRec.TCWK_RISK_LVL       := p_RISK_LEVEL;
      v_TcwkRec.TCWK_NOTES          := NULL;
      v_TcwkRec.TCWK_EFF_START_DATE := v_EffDate;
      v_TcwkRec.TCWK_EDIT_USER_ID   := user;
      v_TcwkRec.TCWK_EDIT_DATE      := sysdate;

      if v_TcwkRec.TCWK_TXMY_FK is null
      then
         raise_application_error( -20001, 'Invalid Taxonomy Code: ->'||p_TAXONOMY_CODE||';');
      end if;

      if v_TcwkRec.TCWK_PTYP_FK is null
      then
         raise_application_error( -20001, 'Invalid Provider Type: ->'||p_PROVIDER_TYPE||';');
      end if;

      if v_TcwkRec.TCWK_SPEC_FK is null
      then
         raise_application_error( -20001, 'Invalid Provider Specialty: ->'||p_SPECIALTY||';');
      end if;

      if v_TcwkRec.TCWK_ENRL_FK is null
      then
         raise_application_error( -20001, 'Invalid Provider Enrollment Type: ->'||p_ENROLLMENT_TYPE||';');
      end if;

      -- Insert the new replacement record
      insert into REF_PRV_SPC_TYP_TAX_ENRL_STAT values v_TcwkRec;
      pSuccessInd := 'Y';

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_CREATE_TXMY_ASSOCIATION;

   ----------------------------------------------------------------
   procedure P_GET_TXMY_ASSOCIATION(p_TAXONOMY_CODE   in out varchar2,
                                    p_ENROLLMENT_TYPE in out varchar2,
                                    p_PROVIDER_TYPE   in out varchar2,
                                    p_SPECIALTY       in out varchar2,
                                    p_RefCursor       out sys_refcursor)
   as
      v_TxmyId  number;
      v_PtypId  number;
      v_SpecId  number;
      v_EnrlId  number;
      v_EffDate date;
      v_EndDate date;

   begin
      v_TxmyId := f_Get_Txmy_Code_Id(p_TAXONOMY_CODE,   v_EffDate);
      v_PtypId := f_Get_Prov_Type_Id(p_PROVIDER_TYPE,   v_EffDate);
      v_SpecId := f_Get_Prov_Spec_Id(p_SPECIALTY,       v_EffDate);
      v_EnrlId := f_Get_Enrl_Type_Id(p_ENROLLMENT_TYPE, v_EffDate);

      open p_RefCursor for
      select tcwk_id_pk                              as "associationId",
             txmy_code                               as "taxonomyCode",
             (select enrl_code
              from REF_ENROLLMENT_TYPE
              where enrl_id_pk = tcwk_enrl_fk) as "enrollmentType",
             (select ptyp_code_desc
              from REF_PROVIDER_TYPE
              where ptyp_id_pk = tcwk_ptyp_fk) as "providerType",
             (select spec_code_desc
              from REF_PROVIDER_SPECIALTY
              where spec_id_pk = tcwk_spec_fk) as "specialty",
             tcwk_risk_lvl                           as "riskLevel",
             'true'                                     as "isTypical",
             to_char(txmy_eff_start_date,
                     'DD-MON-YYYY')                     as "effectiveDate",
             nvl((select to_char(min(txmy_eff_start_date), 'DD-MON-YYYY')
                  from REF_TAXONOMY sb
                  where sb.txmy_code = b.txmy_code
                  and   sb.txmy_eff_start_date > b.txmy_eff_start_date),
                 nvl(to_char(tcwk_deactive_date, 'DD-MON-YYYY'),
                     '31-DEC-2099'))                    as "endDate"
      from REF_PRV_SPC_TYP_TAX_ENRL_STAT a
      join REF_TAXONOMY b
          on txmy_id_pk = tcwk_txmy_fk
      join REF_ENROLLMENT_TYPE c
          on enrl_id_pk = tcwk_enrl_fk
      where decode(nvl(v_TxmyId, 0), 0, 0, tcwk_txmy_fk) = nvl(v_TxmyId, 0)
      and   decode(nvl(v_PtypId, 0), 0, 0, tcwk_ptyp_fk) = nvl(v_PtypId, 0)
      and   decode(nvl(v_SpecId, 0), 0, 0, tcwk_spec_fk) = nvl(v_SpecId, 0)
      and   decode(nvl(v_EnrlId, 0), 0, 0, tcwk_enrl_fk) = nvl(v_EnrlId, 0)
      and   txmy_eff_start_date = (select max(txmy_eff_start_date)
                                      from REF_PRV_SPC_TYP_TAX_ENRL_STAT sa
                                      where sa.tcwk_txmy_fk = a.tcwk_txmy_fk 
                                      and   sa.tcwk_ptyp_fk = a.tcwk_ptyp_fk
                                      and   sa.tcwk_spec_fk = a.tcwk_spec_fk
                                      and   sa.tcwk_enrl_fk = a.tcwk_enrl_fk);

   exception
   when x_CustomError then
      raise;
   when others then
      raise;
   end P_GET_TXMY_ASSOCIATION;

   ----------------------------------------------------------------
   procedure P_UPDATE_TXMY_ASSOCIATION(p_ASSOCIATION_ID  in out number,
                                       p_RISK_LEVEL      in varchar2,
                                       p_IS_TYPICAL      in varchar2,
                                       p_EFFECTIVE_DATE  in varchar2,
                                       p_ACTIVE_DATE     in varchar2,
                                       p_USER_ID         in varchar2,
                                       pSuccessInd       out varchar2)
   as
      v_TcwkRec REF_PRV_SPC_TYP_TAX_ENRL_STAT%rowtype;
      v_EffDate date;

   begin
      pSuccessInd := 'N';
      v_EffDate := to_date(p_EFFECTIVE_DATE, 'MM/DD/YYYY');

      if p_ASSOCIATION_ID is null
      then
         raise_application_error( -20001, 'No association identifier supplied: ->'||p_ASSOCIATION_ID||';');
      end if;

      if p_USER_ID is null
      then
         raise_application_error( -20001, 'No user supplied: ->'||p_USER_ID||';');
      end if;

      select *
      into v_TcwkRec
      from REF_PRV_SPC_TYP_TAX_ENRL_STAT a
      where tcwk_id_pk = p_ASSOCIATION_ID;

      v_TcwkRec.TCWK_ID_PK          := RTDTCWK_TCWK_ID_PK_SEQ.NEXTVAL;
      v_TcwkRec.TCWK_RISK_LVL       := p_RISK_LEVEL;
      v_TcwkRec.TCWK_EFF_START_DATE := v_EffDate;
      v_TcwkRec.TCWK_EDIT_USER_ID   := p_USER_ID;
      v_TcwkRec.TCWK_EDIT_DATE      := sysdate;

      insert into REF_PRV_SPC_TYP_TAX_ENRL_STAT values v_TcwkRec;
      pSuccessInd := 'Y';

      p_ASSOCIATION_ID := v_TcwkRec.TCWK_ID_PK;

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_UPDATE_TXMY_ASSOCIATION;

   ----------------------------------------------------------------
   procedure P_DELETE_TXMY_ASSOCIATION(p_ASSOCIATION_ID in out number,
                                       p_DELETE_DATE    in varchar2,
                                       p_USER_ID        in varchar2,
                                       pSuccessInd      out varchar2)
   as
      v_TcwkRec REF_PRV_SPC_TYP_TAX_ENRL_STAT%rowtype;
      v_EffDate date;

   begin
      pSuccessInd := 'N';
      v_EffDate := to_date(p_DELETE_DATE, 'MM/DD/YYYY');

      if p_ASSOCIATION_ID is null
      then
         raise_application_error( -20001, 'No association identifier supplied: ->'||p_ASSOCIATION_ID||';');
      end if;

      if p_USER_ID is null
      then
         raise_application_error( -20001, 'No user supplied: ->'||p_USER_ID||';');
      end if;

      update REF_PRV_SPC_TYP_TAX_ENRL_STAT a
      set tcwk_deactive_date = v_EffDate,
          tcwk_edit_user_id  = p_USER_ID,
          tcwk_edit_date     = sysdate
      where tcwk_id_pk = p_ASSOCIATION_ID;

      pSuccessInd := 'Y';

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_DELETE_TXMY_ASSOCIATION;

   ----------------------------------------------------------------
   procedure P_UPDATE_TXMY_RISK_LEVEL(p_Taxonomy_Code in varchar2,
                                      p_ProviderType  in varchar2,
                                      p_Specialty     in varchar2,
                                      p_RiskLevel     in varchar2,
                                      p_ModifiedBy    in varchar2,
                                      pSuccessInd     out varchar2)
   as
      v_TcwkRec    REF_PRV_SPC_TYP_TAX_ENRL_STAT%rowtype;
      v_TxmyCodeId number;
      v_ProvTypeId number;
      v_ProvSpecId number;

   begin
      -- Validate input
      if length(p_Taxonomy_Code) < 10
      then
         pSuccessInd := 'N';
         raise_application_error( -20001, 'The Taxonomy code is not a valid length: ->'||p_Taxonomy_Code||';');
      end if;

      v_TxmyCodeId := f_Get_Txmy_Code_Id(p_Taxonomy_Code, sysdate);
      v_ProvTypeId := f_Get_Prov_Type_Id(p_ProviderType,  sysdate);
      v_ProvSpecId := f_Get_Prov_Spec_Id(p_Specialty,     sysdate);

      if v_TxmyCodeId is null
      then
         raise_application_error( -20001, 'Invalid Taxonomy Code: ->'||p_Taxonomy_Code||';');
      end if;

      if v_ProvTypeId is null
      then
         raise_application_error( -20001, 'Invalid Provider Type: ->'||p_ProviderType||';');
      end if;

      if v_ProvSpecId is null
      then
         raise_application_error( -20001, 'Invalid Provider Specialty: ->'||p_Specialty||';');
      end if;

      -- Retrieve the taxonomy crosswalk record
      select REF_PRV_SPC_TYP_TAX_ENRL_STAT.*
      into v_TcwkRec
      from REF_PRV_SPC_TYP_TAX_ENRL_STAT
      where TCWK_TXMY_FK = v_TxmyCodeId
      and   TCWK_PTYP_FK = v_ProvTypeId
      and   TCWK_SPEC_FK = v_ProvSpecId;

      -- Set the new Taxonomy Crosswalk values
      v_TcwkRec.TCWK_ID_PK          := RTDTCWK_TCWK_ID_PK_SEQ.nextval;
      v_TcwkRec.TCWK_RISK_LVL       := p_RiskLevel;
      v_TcwkRec.TCWK_EFF_START_DATE := sysdate;
      v_TcwkRec.TCWK_DEACTIVE_DATE  := NULL;
      v_TcwkRec.TCWK_EDIT_USER_ID   := user;
      v_TcwkRec.TCWK_EDIT_DATE      := sysdate;

      -- Insert the new replacement record
      insert into REF_PRV_SPC_TYP_TAX_ENRL_STAT values v_TcwkRec;
      pSuccessInd := 'Y';

   exception
   when x_CustomError then
      pSuccessInd := 'N';
      raise;
   when no_data_found then
      pSuccessInd := 'N';
      raise;
   when others then
      pSuccessInd := 'N';
      raise;
   end P_UPDATE_TXMY_RISK_LEVEL;

   ----------------------------------------------------------------
   procedure P_TXMY_FETCHBY_ENROLL_TYPE_RC(p_EnrollmentType in varchar2,
                                           p_RefCursor      out sys_refcursor)
   as

   begin
      open p_RefCursor for
      select txmy_code                               as "taxonomyCode",
             (select enrl_code
              from REF_ENROLLMENT_TYPE
              where enrl_id_pk = tcwk_enrl_fk) as "enrollmentCode",
             enrl_code_desc                          as "enrollmentType",
             tcwk_risk_lvl                           as "riskLevel",
             txmy_internal_ind                       as "isInternal",
             to_char(txmy_eff_start_date,
                     'DD-MON-YYYY')                     as "dateActiveFrom",
             txmy_inactive_ind                       as "isInactive",
             (select spec_code_desc
              from REF_PROVIDER_SPECIALTY
              where spec_id_pk = tcwk_spec_fk) as "specialty",
             (select ptyp_code_desc
              from REF_PROVIDER_TYPE
              where ptyp_id_pk = tcwk_ptyp_fk) as "providerType",
             txmy_code_desc                          as "definition",
             nvl((select to_char(min(txmy_eff_start_date), 'DD-MON-YYYY')
                  from REF_TAXONOMY sb
                  where sb.txmy_code = b.txmy_code
                  and   sb.txmy_eff_start_date > b.txmy_eff_start_date),
                 '31-DEC-2099')                         as "dateInactiveFrom",
             txmy_group_name                         as "groupName"
      from REF_PRV_SPC_TYP_TAX_ENRL_STAT a
      join REF_TAXONOMY b
          on txmy_id_pk = tcwk_txmy_fk
      join REF_ENROLLMENT_TYPE c
          on enrl_id_pk = tcwk_enrl_fk
         and enrl_code  = p_EnrollmentType;

   exception
   when others then
      raise;
   end P_TXMY_FETCHBY_ENROLL_TYPE_RC;

   ----------------------------------------------------------------
   procedure P_GET_REF_CODE_BY_TYPE_RC(p_RefTableName in varchar2,
                                       p_RefCursor    out sys_refcursor)
   as
      v_CursorQuery varchar2(5000) := '';
      v_CodeCol     varchar2(30)   := '';
      v_DescCol     varchar2(30)   := '';
      v_EffdCol     varchar2(30)   := '';
      v_DeacCol     varchar2(30)   := '';
      v_ColCnt      number         := 0;

   begin
      select column_name into v_CodeCol from SYS.all_tab_columns where owner = 'RMMISPODS' and table_name = p_RefTableName and column_name like '%_CODE';
      select column_name into v_DescCol from SYS.all_tab_columns where owner = 'RMMISPODS' and table_name = p_RefTableName and column_name like '%_CODE_DESC';
      select column_name into v_EffdCol from SYS.all_tab_columns where owner = 'RMMISPODS' and table_name = p_RefTableName and column_name like '%_EFF_START_DATE';
      select column_name into v_DeacCol from SYS.all_tab_columns where owner = 'RMMISPODS' and table_name = p_RefTableName and column_name like '%_DEACTIVE_DATE';

      v_CursorQuery := 'with curr_rec as
                       (select '||v_CodeCol||'      as curr_code,
                               max('||v_EffdCol||') as curr_eff_date
                        from '||p_RefTableName||'
                        where sysdate between
                              '||v_EffdCol||' and
                              nvl('||v_DeacCol||', to_date('||chr(39)||'12312199'||chr(39)||', '||chr(39)||'MMDDYYYY'||chr(39)||'))
                        group by '||v_CodeCol||'
                       )
                       select '||v_DescCol||' AS "label", '||v_CodeCol||' AS "value"
                       from '||p_RefTableName||'
                       join CURR_REC
                           on CURR_CODE     = '||v_CodeCol||'
                          and CURR_EFF_DATE = '||v_EffdCol||'
                       order by '||v_DescCol;
      open p_RefCursor for v_CursorQuery;

   exception
      when others then
         raise;
   end P_GET_REF_CODE_BY_TYPE_RC;

   ----------------------------------------------------------------
   -- p_IsDeadInd = 'Y' then he's dead
   -- p_IsDeadInd = 'N' then he got better
   ----------------------------------------------------------------
   procedure P_VERIFY_IN_DMF(p_Ssn       in varchar2,
                             p_RefCursor out sys_refcursor)
   as
      v_Ssn varchar2(10);

   begin
      v_Ssn := replace(p_Ssn, '-');

      open p_RefCursor for
      select DMFR_LAST_NAME      as "lastName",
             DMFR_NAME_SUFF      as "nameSuffix",
             DMFR_FIRST_NAME     as "firstName",
             DMFR_MIDDLE_NAME    as "middleName",
             DMFR_V_P_CODE       as "vpCode",
             DMFR_BIRTH_DATE_TXT as "birthDate",
             DMFR_DEATH_DATE_TXT as "deathDate"
--             to_char(DMFR_BIRTH_DATE, 'DD-MON-YYYY') as "birthDate",
--             to_char(DMFR_DEATH_DATE, 'DD-MON-YYYY') as "deathDate"
      from REF_DEATH_MASTER_FILE
      where dmfr_ssn = v_Ssn;

   end P_VERIFY_IN_DMF;

   ----------------------------------------------------------------
   procedure P_VERIFY_IN_DMF_Name(p_first_name in  varchar2,
                                  p_last_name  in  varchar2,
                                  p_RefCursor  out sys_refcursor)
   as
      v_first_name varchar2(100);
      v_last_name varchar2(100);
   begin
      open p_RefCursor for
      select DMFR_SSN             as "SSN",
             DMFR_NAME_SUFF       as "nameSuffix",
             DMFR_MIDDLE_NAME     as "middleName",
             DMFR_V_P_CODE        as "vpCode",
             DMFR_BIRTH_DATE_TXT  as "birthDate",
             DMFR_DEATH_DATE_TXT  as "deathDate"
      from RMMISPODS.REF_DEATH_MASTER_FILE
      where UPPER(DMFR_LAST_NAME) LIKE v_last_name
      and   UPPER(DMFR_FIRST_NAME) LIKE v_first_name;

   end P_VERIFY_IN_DMF_Name;

   ----------------------------------------------------------------
   procedure P_VERIFY_IN_DEA_REGISTRATION(p_registration in  varchar2,
                                          p_RefCursor    out sys_refcursor)
   as

   begin
      open p_RefCursor for
      select DEAR_REGISTRATION_NUMBER                    as "registrationNumber",
             DEAR_BUS_ACTVITY_CODE                       as "businessActivityCode",
             DEAR_DRUG_SCHEDULES                         as "drugSchedules",
             TO_CHAR(DEAR_EXPIRATION_DATE, 'MM-DD-YYYY') as "expirationDate",
             DEAR_NAME                                   as "name",
             DEAR_ADDITONAL_COMPANY_INFO                 as "additionalCompanyInfo",
             DEAR_ADDRESS_1                              as "address1",
             DEAR_ADDRESS_2                              as "address2",
             DEAR_CITY                                   as "city",
             DEAR_STATE                                  as "state",
             DEAR_ZIP_CODE                               as "zipCode",
             DEAR_BUS_ACTIVITY_SUB_CODE                  as "businessActivitySubCode",
             DEAR_PAYMENT_INDCATOR                       as "paymentIndicator",
             DEAR_ACTIVITY                               as "activity",
             DEAR_EDIT_USER_ID                           as "editUserId",
             TO_CHAR(DEAR_EDIT_DATE, 'MM-DD-YYYY')       as "editDate"
      from RMMISPODS.REF_DEA_REGISTRATION
      where DEAR_REGISTRATION_NUMBER = p_registration;

   end P_VERIFY_IN_DEA_REGISTRATION;

   ----------------------------------------------------------------
   procedure P_GET_LICENSE_RC(p_LicenseType     in  varchar2,
                              p_LicenseCategory in  varchar2,
                              p_RefCursor       out sys_refcursor)
   as

   begin
      if p_LicenseType is null
      then
         open p_RefCursor for
         with curr_rec as
         (select ltyp_code                as LTYP_CODE,
                 max(ltyp_eff_start_date) as START_DATE
          from REF_LICENSE_TYPE
          where ltyp_eff_start_date <= sysdate
          group by ltyp_code)
         select null           as licn_id_pk,
                ltyp_id_pk     as ltyp_id_pk,
                ltyp_code_desc as ltyp_code_desc,
                null           as lcat_id_pk,
                null           as lcat_code_desc,
                null           as ldes_id_pk,
                null           as ldes_code_desc
         from REF_LICENSE_TYPE a
         join curr_rec b
             on b.ltyp_code  = a.ltyp_code
            and b.start_date = a.ltyp_eff_start_date;

        return;
      elsif p_LicenseCategory is null
      then
         open p_RefCursor for
         with curr_rec as
         (select lcat_code                as LCAT_CODE,
                 max(lcat_eff_start_date) as START_DATE
          from REF_LICENSE_CATEGORY
          where lcat_eff_start_date <= sysdate
          group by lcat_code)
         select distinct null  as licn_id_pk,
                ltyp_id_pk     as ltyp_id_pk,
                ltyp_code_desc as ltyp_code_desc,
                lcat_id_pk     as lcat_id_pk,
                lcat_code_desc as lcat_code_desc,
                null           as ldes_id_pk,
                null           as ldes_code_desc
         from REF_LICENSE_CATEGORY a
         join curr_rec b
             on b.lcat_code  = a.lcat_code
            and b.start_date = a.lcat_eff_start_date
         join REF_LICENSE_DESC_TYPE_CATEGORY
             on licn_lcat_id_fk = lcat_id_pk
         join REF_LICENSE_TYPE
             on ltyp_id_pk     = licn_ltyp_id_fk
            and ltyp_code_desc = p_LicenseType
         ;

        return;
      else
         open p_RefCursor for
         with curr_rec as
         (select licn_id_pk               as LICN_ID,
                 max(licn_eff_start_date) as START_DATE
          from REF_LICENSE_DESC_TYPE_CATEGORY
          where licn_eff_start_date <= sysdate
          group by licn_id_pk)
         select licn_id_pk     as licn_id_pk,
                ltyp_id_pk     as ltyp_id_pk,
                ltyp_code_desc as ltyp_code_desc,
                lcat_id_pk     as lcat_id_pk,
                lcat_code_desc as lcat_code_desc,
                ldes_id_pk     as ldes_id_pk,
                ldes_code_desc as ldes_code_desc
         from REF_LICENSE_DESC_TYPE_CATEGORY
         join curr_rec
             on licn_id    = licn_id_pk
            and start_date = licn_eff_start_date
         join REF_LICENSE_TYPE
             on ltyp_id_pk = licn_ltyp_id_fk
         join REF_LICENSE_CATEGORY
             on lcat_id_pk = licn_lcat_id_fk
         join REF_LICENSE_DESCRIPTION
             on ldes_id_pk = licn_ldes_id_fk
         where ltyp_code_desc = p_LicenseType
         and   lcat_code_desc = p_LicenseCategory;
      end if;


   exception
      when others then
         raise;
   end P_GET_LICENSE_RC;

   ----------------------------------------------------------------
   procedure P_GET_CERT_TYPE_RC(p_RefCursor out sys_refcursor)
   as

   begin
      open p_RefCursor for
      with curr_rec as
         (select ctyp_code                as CTYP_CODE,
                 max(ctyp_eff_start_date) as START_DATE
          from REF_CERTIFICATION_TYPE
          where ctyp_eff_start_date <= sysdate
          group by ctyp_code)
         select ctyp_id_pk,
                ctyp_code_desc
         from REF_CERTIFICATION_TYPE a
         join curr_rec b
             on b.ctyp_code  = a.ctyp_code
            and b.start_date = a.ctyp_eff_start_date;

   exception
      when others then
         raise;
   end P_GET_CERT_TYPE_RC;

   ----------------------------------------------------------------
   procedure P_GET_CERT_ISS_ENT_RC(p_RefCursor out sys_refcursor)
   as

   begin
      open p_RefCursor for
      with curr_rec as
         (select cien_code                as CIEN_CODE,
                 max(cien_eff_start_date) as START_DATE
          from REF_CERTIFICATION_ISS_ENTITY
          where cien_eff_start_date <= sysdate
          group by cien_code)
         select cien_id_pk,
                cien_code_desc
         from REF_CERTIFICATION_ISS_ENTITY a
         join curr_rec b
             on b.cien_code  = a.cien_code
            and b.start_date = a.cien_eff_start_date;

   exception
      when others then
         raise;
   end P_GET_CERT_ISS_ENT_RC;

   ----------------------------------------------------------------
   function F_GET_CITY_STATE_LIST(p_StateCode in varchar2)
   return sys_refcursor
   as
      v_ListCur sys_refcursor;

   begin
      g_RowCnt := 0;

      open v_ListCur for
         select CSLU_CITY_NAME,
                CSLU_STATE_CODE,
                cslu_msa_ind
         from REF_CITY_LOOKUP
         where CSLU_STATE_CODE = p_StateCode;

      return v_ListCur;

   end F_GET_CITY_STATE_LIST;

   ----------------------------------------------------------------
   function F_IS_VALID_CITY(p_CityName   varchar2,
                            p_StateCode varchar2)
   return varchar2
   as
      v_ValidInd varchar2(1) := 'N';

   begin
      if length(p_CityName)  > 0 and
         length(p_StateCode) > 0
      then
         select 'Y'
         into v_ValidInd
         from REF_CITY_LOOKUP
         where upper(cslu_city_name)  = upper(p_CityName)
         and   upper(cslu_state_code) = upper(p_StateCode);
      else
         v_ValidInd := 'N';
      end if;

      return v_ValidInd;

   exception
      when no_data_found then
         return 'N';
      when others then
         return 'N';
   end F_IS_VALID_CITY;

   ----------------------------------------------------------------
   function F_IS_VALID_MSA_CITY(p_CityName  varchar2,
                                p_StateCode varchar2)
   return varchar2
   as
      v_ValidInd varchar2(1) := 'N';

   begin
      if length(p_CityName)  > 0 and
         length(p_StateCode) > 0
      then
         select cslu_msa_ind
         into v_ValidInd
         from REF_CITY_LOOKUP
         where upper(cslu_city_name)  = upper(p_CityName)
         and   upper(cslu_state_code) = upper(p_StateCode);
      else
         v_ValidInd := 'N';
      end if;

      return v_ValidInd;

   exception
      when no_data_found then
         return 'N';
      when others then
         return 'N';
   end F_IS_VALID_MSA_CITY;

   end PKG_REFERENCE_DATA;
/

show errors;

create or replace public synonym PKG_REFERENCE_DATA for RMMISPODS.PKG_REFERENCE_DATA
/

grant execute on PKG_REFERENCE_DATA to RMMIS_APPLICATION_USER
/
