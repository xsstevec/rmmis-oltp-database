ALTER SESSION SET CURRENT_SCHEMA = rmmispods;

create or replace
package RMMISPODS.PKG_TRANSACTION_CONTROL as
   ---------------------------------------------------------------------
   -- Procedure to insert a record in to the transaction control table.
   ---------------------------------------------------------------------
   procedure P_ADD_TRANSACTION_RECORD(p_TransactionId in varchar2,
                                      p_TableOwner    in varchar2,
                                      p_TableName     in varchar2,
                                      p_RowId         in rowid);

   ---------------------------------------------------------------------
   -- Procedure to finalize the transaction.
   ---------------------------------------------------------------------
   procedure P_COMMIT_TRANSACTION(p_TransactionId in varchar2);

   ---------------------------------------------------------------------
   -- Procedure to undo the transaction.
   ---------------------------------------------------------------------
   procedure P_ROLLBACK_TRANSACTION(p_TransactionId in varchar2);

end PKG_TRANSACTION_CONTROL;
/

show errors;

create or replace package body RMMISPODS.PKG_TRANSACTION_CONTROL as
   ----------------------------------------------------------------
   -- Private global variables
   ----------------------------------------------------------------
   g_TraceMsg varchar2(155);

   x_CustomError exception;
   PRAGMA EXCEPTION_INIT(x_CustomError, -20001 );

   ----------------------------------------------------------------
   -- Private procedures and functions
   ----------------------------------------------------------------
   ---------------------------------------------------------------------
   -- Function to to retrieve the correct sequence number for a
   -- transaction.
   ---------------------------------------------------------------------
   function F_GET_NEXT_TRANS_SEQ(p_TransactionId varchar2)
   return number
   is
      v_Sequence number := 0;

   begin
      g_TraceMsg := 'F_GET_NEXT_TRANS_SEQ: Set sequence value';
      select nvl(max(xact_insert_order), -1) + 1
      into v_Sequence
      from mgt_transaction
      where xact_id = p_TransactionId;

      return v_Sequence;

   exception
      when no_data_found then
         return 0;
      when others then
         raise;
   end F_GET_NEXT_TRANS_SEQ;
   
   ----------------------------------------------------------------
   -- Public procedures and functions
   ----------------------------------------------------------------
   ---------------------------------------------------------------------
   -- Procedure to insert a record in to the transaction control table.
   ---------------------------------------------------------------------
   procedure P_ADD_TRANSACTION_RECORD(p_TransactionId in varchar2,
                                         p_TableOwner    in varchar2,
                                         p_TableName     in varchar2,
                                         p_RowId         in rowid)
   is
      v_Sequence number := 0;

   begin
      -- Set the sequence number for the transaction
      v_Sequence := F_GET_NEXT_TRANS_SEQ(p_TransactionId);

      -- Insert the transaction data
      g_TraceMsg := 'P_ADD_TRANSACTION_RECORD: Insert record';
      insert into MGT_TRANSACTION
      (XACT_ID,
       XACT_INSERT_ORDER,
       XACT_TABLE_OWNER,
       XACT_TABLE_NAME,
       XACT_ROWID)
      values
      (p_TransactionId,
       v_Sequence,
       p_TableOwner,
       p_TableName,
       p_RowId);

   exception
      when others then
         dbms_output.put_line(g_TraceMsg);
         raise;
   end P_ADD_TRANSACTION_RECORD;

   ---------------------------------------------------------------------
   -- Procedure to finalize the transaction.
   ---------------------------------------------------------------------
   procedure P_COMMIT_TRANSACTION(p_TransactionId in varchar2)
   is

   begin
      g_TraceMsg := 'P_COMMIT_TRANSACTION: Update commit columns';
      update MGT_TRANSACTION
      set XACT_ROW_COMMIT_DATE = sysdate,
          XACT_ROW_COMMIT_USER = user
      where XACT_ID = p_TransactionId
      and   XACT_ROW_COMMIT_DATE is null;

      g_TraceMsg := 'P_COMMIT_TRANSACTION: Commit';
      commit;

   exception
      when others then
         dbms_output.put_line(g_TraceMsg);
         raise;
   end P_COMMIT_TRANSACTION;

   ---------------------------------------------------------------------
   -- Procedure to undo the transaction.
   ---------------------------------------------------------------------
   procedure P_ROLLBACK_TRANSACTION(p_TransactionId in varchar2)
   is
      v_TabOwner MGT_TRANSACTION.XACT_TABLE_OWNER%type;
      v_TabName  MGT_TRANSACTION.XACT_TABLE_NAME%type;
      v_TabRowid MGT_TRANSACTION.XACT_ROWID%type;

      cursor main_cur is
         select XACT_TABLE_OWNER,
                XACT_TABLE_NAME,
                XACT_ROWID
         from MGT_TRANSACTION
         where XACT_ID = p_TransactionId
         and   XACT_ROW_COMMIT_DATE is null
         order by XACT_INSERT_ORDER desc
         for update;

   begin
      g_TraceMsg := 'P_ROLLBACK_TRANSACTION: Open cursor';
      open main_cur;

      loop
         fetch main_cur
         into v_TabOwner,
              v_TabName,
              v_TabRowid;
         exit when main_cur%notfound;

      begin
         g_TraceMsg := 'P_ROLLBACK_TRANSACTION: Delete: '||
                       v_TabOwner||'.'||v_TabName||
                       '.'||v_TabRowid;
         execute immediate 'delete from '||v_TabOwner||'.'||
                           v_TabName||' where rowid = '||
                           chr(39)||v_TabRowid||chr(39);

---         g_TraceMsg := 'P_ROLLBACK_TRANSACTION: Delete record';
---         delete from MGT_TRANSACTION where current of main_cur;
---
         g_TraceMsg := 'P_ROLLBACK_TRANSACTION: Mark as rolled back';
         update MGT_TRANSACTION
         set XACT_ROW_ROLLBACK_DATE = sysdate,
             XACT_ROW_ROLLBACK_USER = user
         where current of main_cur;

      exception
         when others then
            if sqlcode <> -2292   -- Ignore child record found error
            then
               raise;
            end if;
      end;
      end loop;

      g_TraceMsg := 'P_ROLLBACK_TRANSACTION: Commit';
      commit;

      close main_cur;

   exception
      when others then
         dbms_output.put_line(g_TraceMsg);
         close main_cur;
         rollback;
         raise;
   end P_ROLLBACK_TRANSACTION;

end PKG_TRANSACTION_CONTROL;
/

show errors;

grant execute on PKG_TRANSACTION_CONTROL to RMMIS_APPLICATION_USER
/

